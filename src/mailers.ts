import {z} from "zod";
import {indent, makeEntry, passthrough, stringToString, stringZod, timeZod} from "./utils";

const name = makeEntry(
    'name',
    stringZod.describe(`<mailersect>: Creates a new mailer list with the name <mailersect>. It is an independent section which is referenced by one or more proxies`),
    passthrough('mailers'),
);

const mailer = makeEntry(
    'mailer',
    z.array(z.object({
        name: stringZod,
        ip: stringZod,
        port: z.number().max(65565),
    })).optional().describe(`<mailername> <ip>:<port>: Defines a mailer inside a mailers section`),
    (v) => v ? v.map((e) => `mailer ${stringToString(e.name)} ${stringToString(e.ip)}:${e.port}`).join('\n') : '',
);

const timeout = makeEntry(
    'timeout',
    timeZod.optional().describe(`<time>: Defines the time available for a mail/connection to be made and send to the mail-server. If not defined the default value is 10 seconds. To allow for at least two SYN-ACK packets to be send during initial TCP handshake it is advised to keep this value above 4 seconds.`),
    (v) => v ? passthrough('timeout mail')(v) : '',
);

const mailersEntry = z.array(z.object({
    name: name.validator,
    mailer: mailer.validator,
    timeout: timeout.validator,
}));

const mailers = makeEntry(
    'mailers',
    mailersEntry,
    (v) => v.map((e) => {
        let out = name.generator(e.name);
        if (e.mailer) out += `\n${indent(mailer.generator(e.mailer))}`;
        if (e.timeout) out += `\n${indent(timeout.generator(e.timeout))}`;
        return out;
    }).join('\n'),
);

export default {
    generator: mailers.generator,
    validator: mailers.validator,
};
