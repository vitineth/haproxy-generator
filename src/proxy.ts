import {
    Entry, entryToObject,
    ifUnlessCondition, indent,
    makeEntry, makeGenerator,
    optional,
    passthrough, sizeZod,
    stringArrayPassthrough,
    stringToString,
    stringZod,
    timeZod
} from "./utils";
import {z} from "zod";


const todo = () => {
    return '';
    // throw new Error('Not yet implemented');
}

const acl = makeEntry(
    "acl",
    z.array(z.object({
        name: stringZod,
        criteria: stringZod,
    })).optional().describe(`<aclname> <criterion> [flags] [operator] <value> ...: Declare or complete an access list`),
    (v) => v ? v.map((e) => `acl ${stringToString(e.name)} ${stringToString(e.criteria)}`).join('\n') : '',
);

const backlog = makeEntry(
    "backlog",
    z.number().optional().describe(`<conns>: Give hints to the system about the approximate listen backlog desired size <conns>   is the number of pending connections. Depending on the operating system, it may represent the number of already acknowledged connections, of non-acknowledged ones, or both. In order to protect against SYN flood attacks, one solution is to increase the system's SYN backlog size. Depending on the system, sometimes it is just tunable via a system parameter, sometimes it is not adjustable at all, and sometimes the system relies on hints given by the application at the time of the listen() syscall. By default, HAProxy passes the frontend's maxconn value to the listen() syscall. On systems which can make use of this value, it can sometimes be useful to be able to specify a different value, hence this backlog parameter. On Linux 2.4, the parameter is ignored by the system. On Linux 2.6, it is used as a hint and the system accepts up to the smallest greater power of two, and never more than some limits (usually 32768).`),
    (v) => v ? passthrough('backlog')(v) : '',
);

const balance = makeEntry(
    "balance",
    z.object({
        algorithm: z.enum(['roundrobin', 'static-rr', 'leastconn', 'first', 'source', 'uri', 'url_param', 'random', 'rdp-cookie'])
            .or(z.object({
                type: z.literal('hdr'),
                name: stringZod,
            }))
            .or(z.object({
                type: z.literal('random'),
                draws: z.number(),
            }))
            .or(z.object({
                type: z.literal('rdp-cookie'),
                name: stringZod,
            })),
        arguments: z.array(stringZod).optional(),
    })
        .or(z.object({
            urlParam: stringZod,
            checkPost: z.boolean().optional(),
        })).optional(),
    (v) => {
        if (!v) return '';
        if ('algorithm' in v) {
            if (typeof (v.algorithm) === 'object' && 'type' in v.algorithm) {
                switch (v.algorithm.type) {
                    case 'hdr':
                        return `balance ${v.algorithm.type}(${stringToString(v.algorithm.name)}) ${(v.arguments ?? []).map(stringToString).join(' ')}`;
                    case "random":
                        return `balance ${v.algorithm.type}(${v.algorithm.draws}) ${(v.arguments ?? []).map(stringToString).join(' ')}`;
                    case 'rdp-cookie':
                        return `balance ${v.algorithm.type}(${stringToString(v.algorithm.name)}) ${(v.arguments ?? []).map(stringToString).join(' ')}`;
                }
            } else {
                return `balance ${v.algorithm} ${(v.arguments ?? []).map(stringToString).join(' ')}`;
            }
        } else {
            return `balance url_param ${v.urlParam} ${v.checkPost ? 'check_post' : ''}`;
        }
    },
);

const bind = makeEntry(
    "bind",
    z.array(
        z.object({
            addresses: z.array(z.object({
                address: stringZod,
                port: z.number().or(z.object({
                    min: z.number(),
                    max: z.number(),
                }))
            })),
            params: z.array(stringZod).optional(),
        }).or(z.object({
            paths: z.array(stringZod),
            params: z.array(stringZod).optional(),
        }))
    ).optional(),
    (v) => v ? v.map((e) => {
        if ('addresses' in e) {
            return 'bind ' + e.addresses.map((a) => {
                if (typeof (a.port) === 'number') {
                    return `${stringToString(a.address)}:${a.port}`;
                } else {
                    return `${stringToString(a.address)}:${a.port.min}-${a.port.max}`;
                }
            }).join(',') + ' ' + (e.params ?? []).map(stringToString).join(' ');
        } else {
            return 'bind ' + e.paths.map(stringToString).join(',') + ' ' + (e.params ?? []).map(stringToString).join(' ');
        }
    }).join('\n') : '',
);

const bindProcess = makeEntry(
    "bind-process",
    z.array(z.string().regex(/(all|odd|even|[0-9]+(-([0-9]+)?)?)/)).optional(),
    (v) => v ? v.map(passthrough('bind-process')).join('\n') : '',
);

const block = makeEntry(
    "block",
    z.array(z.object({
        if: stringZod,
    }).or(z.object({
        unless: stringZod,
    }))).optional(),
    (v) => v ? v.map((e) => {
        if ('if' in e) return `block if ${e.if}`;
        else return `block unless ${e.unless}`;
    }).join('\n') : '',
);

const captureCookie = makeEntry(
    "capture cookie",
    z.array(z.object({
        name: stringZod,
        length: z.number(),
    })).optional(),
    (v) => v ? v.map((e) => `capture cookie ${stringToString(e.name)} len ${e.length}`).join('\n') : '',
);

const captureRequestHeader = makeEntry(
    "capture request header",
    z.array(z.object({
        name: stringZod,
        length: z.number(),
    })).optional(),
    (v) => v ? v.map((e) => `capture request header ${stringToString(e.name)} len ${e.length}`).join('\n') : '',
);

const captureResponseHeader = makeEntry(
    "capture response header",
    z.array(z.object({
        name: stringZod,
        length: z.number(),
    })).optional(),
    (v) => v ? v.map((e) => `capture response header ${stringToString(e.name)} len ${e.length}`).join('\n') : '',
);

const clitimeout = makeEntry(
    "clitimeout",
    z.number().optional(),
    (v) => v ? passthrough('clitimeout')(v) : '',
);

const compression = makeEntry(
    "compression",
    z.array(z.literal('offload').or(z.object({
        algo: z.array(stringZod),
    })).or(z.object({
        type: z.array(stringZod),
    }))).optional(),
    (v) => v ? v.map((e) => {
        if (e === 'offload') return `compression offload`;
        else if ('algo' in e) return `compression algo ${e.algo.map(stringToString).join(' ')}`;
        else return `compression type ${e.type.map(stringToString).join(' ')}`;
    }).join('\n') : '',
);

const contimeout = makeEntry(
    "contimeout",
    timeZod.optional(),
    (v) => v ? passthrough('contimeout')(v) : '',
);

const cookie = makeEntry(
    "cookie",
    z.array(z.object({
        name: stringZod,
        source: z.enum(['rewrite', 'insert', 'prefix']).optional(),
        indirect: z.boolean().optional(),
        nocache: z.boolean().optional(),
        postonly: z.boolean().optional(),
        preserve: z.boolean().optional(),
        httponly: z.boolean().optional(),
        secure: z.boolean().optional(),
        domain: z.array(stringZod).optional(),
        maxidle: timeZod.optional(),
        maxlife: timeZod.optional(),
        dynamic: z.boolean().optional(),
        attr: z.array(stringZod).optional(),
    })).optional(),
    (v) => v ? v.map((e) => {
        let out = `cookie ${stringToString(e.name)}`;
        if (e.source) out += ` ${e.source}`;
        if (e.indirect) out += ` indirect`;
        if (e.nocache) out += ` nocache`;
        if (e.postonly) out += ` postonly`;
        if (e.preserve) out += ` preserve`;
        if (e.httponly) out += ` httponly`;
        if (e.secure) out += ` secure`;
        if (e.domain) out += ` ${e.domain.map((y) => 'domain ' + stringToString(y)).join(' ')}`;
        if (e.maxidle) out += ` maxidle ${typeof (e.maxidle) === 'number' ? e.maxidle : stringToString(e.maxidle)}`;
        if (e.maxlife) out += ` maxlife ${typeof (e.maxlife) === 'number' ? e.maxlife : stringToString(e.maxlife)}`;
        if (e.dynamic) out += ` dynamic`;
        if (e.attr) out += ` ${e.attr.map((y) => 'attr ' + stringToString(y)).join(' ')}`;
        return out;
    }).join('\n') : '',
);

const declareCapture = makeEntry(
    "declare capture",
    z.array(z.object({
        source: z.enum(['request', 'response']),
        length: z.number(),
    })).optional(),
    (v) => v ? v.map((e) => `declare capture ${e.source} len ${e.length}`).join('\n') : '',
);

const defaultServer = makeEntry(
    "default-server",
    z.array(z.array(stringZod)).optional(),
    (v) => v ? v.map((e) => `default-server ${e.map(stringToString).join(' ')}`).join('\n') : '',
);

const defaultBackend = makeEntry(
    "default_backend",
    stringZod.optional(),
    (v) => v ? passthrough('default_backend')(v) : '',
);

const description = makeEntry(
    "description",
    stringZod.optional(),
    (v) => v ? passthrough('description')(v) : '',
);

const disabled = makeEntry(
    "disabled",
    z.boolean().optional(),
    (v) => v ? optional('disabled')(v) : '',
);

const dispatch = makeEntry(
    "dispatch",
    z.object({
        address: stringZod,
        port: z.number(),
    }).optional(),
    (v) => v ? `dispatch ${stringToString(v.address)}:${v.port}` : '',
);

const dynamicCookieKey = makeEntry(
    "dyanmic-cookie-key",
    stringZod.optional(),
    (v) => v ? `dynamic-cookie-key ${stringToString(v)}` : '',
);

const emailAlertFrom = makeEntry(
    "email-alert from",
    stringZod.optional(),
    (v) => v ? passthrough('email-alert from')(v) : '',
);

const emailAlertLevel = makeEntry(
    "email-alert level",
    z.enum(['emerg', 'alert', 'crit', 'err', 'warning', 'notice', 'info', 'debug']).optional(),
    (v) => v ? passthrough('email-alert level')(v) : '',
);

const emailAlertMailers = makeEntry(
    "email-alert mailers",
    z.array(stringZod).optional(),
    (v) => v ? v.map((e) => `email-alert mailers ${stringToString(e)}`).join('\n') : '',
);

const emailAlertMyhostname = makeEntry(
    "email-alert myhostname",
    stringZod.optional(),
    (v) => v ? `email-alert myhostname ${stringToString(v)}` : '',
);

const emailAlertTo = makeEntry(
    "email-alert to",
    z.array(stringZod).optional(),
    (v) => v ? v.map((e) => `email-alert to ${stringToString(e)}`).join('\n') : '',
);

const enabled = makeEntry(
    "enabled",
    z.boolean().optional(),
    (v) => v ? optional('enabled')(v) : '',
);

const errorfile = makeEntry(
    "errorfile",
    z.array(z.object({
        code: z.number(),
        file: stringZod,
    })).optional(),
    (v) => v ? v.map((e) => `errorfile ${e.code} ${stringToString(e.file)}`).join('\n') : '',
);

const errorloc = makeEntry(
    "errorloc",
    z.array(z.object({
        code: z.number(),
        url: stringZod,
    })).optional(),
    (v) => v ? v.map((e) => `errorloc ${e.code} ${stringToString(e.url)}`).join('\n') : '',
);

const errorloc302 = makeEntry(
    "errorloc302",
    z.array(z.object({
        code: z.number(),
        url: stringZod,
    })).optional(),
    (v) => v ? v.map((e) => `errorloc302 ${e.code} ${stringToString(e.url)}`).join('\n') : '',
);

const errorloc303 = makeEntry(
    "errorloc303",
    z.array(z.object({
        code: z.number(),
        url: stringZod,
    })).optional(),
    (v) => v ? v.map((e) => `errorloc303 ${e.code} ${stringToString(e.url)}`).join('\n') : '',
);

const forcePersist = makeEntry(
    "force-persist",
    z.array(z.object({
        if: stringZod,
    }).or(z.object({
        unless: stringZod,
    }))).optional(),
    (v) => v ? v.map((e) => {
        if ('if' in e) return `force-persist if ${stringToString(e.if)}`;
        else return `force-persist unless ${stringToString(e.unless)}`;
    }).join('\n') : '',
);

const filter = makeEntry(
    "filter",
    z.array(stringZod.or(z.object({
        name: stringZod,
        params: z.array(stringZod).optional(),
    }))).optional(),
    (v) => v ? v.map((e) => {
        if (typeof (e) === 'object' && 'name' in e) return `filter ${stringToString(e.name)} ${e.params ? e.params.map(stringToString).join(' ') : ''}`;
        else return `filter ${stringToString(e)}`;
    }).join('\n') : '',
);

const fullconn = makeEntry(
    "fullconn",
    z.number().optional(),
    (v) => v !== undefined ? `fullconn ${v}` : '',
);

const grace = makeEntry(
    "grace",
    timeZod.optional(),
    (v) => v !== undefined ? passthrough('grace')(v) : '',
);

const hashBalanceFactor = makeEntry(
    "hash-balance-factor",
    z.number().optional(),
    (v) => v !== undefined ? `hash-balance-factor ${v}` : '',
)

const hashType = makeEntry(
    "hash-type",
    z.array(z.object({
        method: z.enum(['map-based', 'consistent']),
        function: z.enum(['sdbm', 'djb2', 'wt6', 'crc32']),
        modifier: z.literal('avalanche'),
    })).optional(),
    (v) => v ? v.map((e) => `hash-type ${e.method} ${e.function} ${e.modifier}`).join('\n') : '',
);

const httpCheckDisableOn404 = makeEntry(
    "http-check disable-on-404",
    z.boolean().optional(),
    (v) => v !== undefined ? optional('http-check disable-on-404')(v) : '',
);

const httpCheckExpect = makeEntry(
    "http-check expect",
    z.array(z.object({
        invert: z.boolean().optional(),
        match: stringZod,
        pattern: stringZod,
    })).optional(),
    (v) => v ? v.map((e) => `http-check expect${e.invert ? ' ! ' : ' '}${stringToString(e.match)} ${stringToString(e.pattern)}`).join('\n') : '',
);

const httpCheckSend = makeEntry(
    "http-check send",
    z.array(z.object({
        headers: z.array(z.object({
            name: stringZod,
            value: stringZod,
        })).optional(),
        body: stringZod.optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-check send${e.headers ? e.headers.map((d) => `hdr ${stringToString(d.name)} ${stringToString(d.value)}`).join(' ') : ''} ${e.body ? `body ${stringToString(e.body)}` : ''}`).join('\n') : '',
);

const httpCheckSendState = makeEntry(
    "http-check send-state",
    z.boolean().optional(),
    (v) => v !== undefined ? optional('http-check send-state')(v) : '',
);

const httpRequest = makeEntry(
    "http-request",
    z.array(z.object({
        action: stringZod,
        options: z.array(stringZod).optional(),
        condition: z.object({
            if: stringZod
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => {
        let out = `http-request ${stringToString(e.action)}`;
        if (e.options) out += ` ${e.options.map(stringToString).join(' ')}`;
        if (e.condition) {
            if ('if' in e.condition) out += ` if ${stringToString(e.condition.if)}`;
            else out += ` unless ${stringToString(e.condition.unless)}`;
        }
        return out;
    }).join('\n') : '',
);

const httpRequestAddAcl = makeEntry(
    'http-request add-acl',
    z.array(z.object({
        filename: stringZod,
        keyfmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => {
        let out = `http-request add-acl(${stringToString(e.filename)}) ${stringToString(e.keyfmt)}`;
        if (e.condition) {
            if ('if' in e.condition) {
                out += ` if ${stringToString(e.condition.if)}`;
            } else {
                out += ` unless ${stringToString(e.condition.unless)}`;
            }
        }
        return out;
    }).join('\n') : '',
);

const httpRequestAddHeader = makeEntry(
    'http-request add-header',
    z.array(z.object({
        name: stringZod,
        fmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request add-header ${stringToString(e.name)} ${stringToString(e.fmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestAllow = makeEntry(
    'http-request allow',
    z.array(z.literal(true).or(z.object({
        if: stringZod,
    }).or(z.object({
        unless: stringZod,
    })))).optional(),
    (v) => v ? v.map((e) => e === true ? `http-request allow` : `http-request allow ${ifUnlessCondition(e)}`).join('\n') : '',
);

const httpRequestAuth = makeEntry(
    'http-request auth',
    z.array(z.object({
        realm: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request auth ${e.realm ? 'realm ' + stringToString(e.realm) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestCacheUse = makeEntry(
    'http-request cache-use',
    z.array(z.object({
        name: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request cache-use ${stringToString(e.name)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestCapture = makeEntry(
    'http-request capture',
    z.array(z.object({
        sample: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        filter: z.object({len: z.number()}).or(z.object({id: stringZod})).optional(),
    })).optional(),
    (v) => v ? v.map((e) => {
        let out = `http-request capture ${stringToString(e.sample)}`;
        if (e.filter) {
            if ('len' in e.filter) out += ` len ${e.filter.len}`;
            else out += ` id ${e.filter.id}`;
        }
        out += ` ${ifUnlessCondition(e.condition)}`;
        return out;
    }).join('\n') : '',
);

const httpRequestDelAcl = makeEntry(
    'http-request del-acl',
    z.array(z.object({
        filename: stringZod,
        keyfmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request del-acl(${stringToString(e.filename)}) ${stringToString(e.keyfmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestDelHeader = makeEntry(
    'http-request del-header',
    z.array(z.object({
        name: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request del-header ${stringToString(e.name)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestDelMap = makeEntry(
    'http-request del-map',
    z.array(z.object({
        filename: stringZod,
        keyfmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request del-map(${stringToString(e.filename)}) ${stringToString(e.keyfmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestDeny = makeEntry(
    'http-request deny',
    z.array(z.literal(true).or(z.object({
        denyStatus: z.number().optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    }))).optional(),
    (v) => v ? v.map((e) => e === true ? `http-request deny` : `http-request deny ${e.denyStatus !== undefined ? `deny_status ${e.denyStatus}` : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestDisable17Retry = makeEntry(
    'http-request disable-17-retry',
    z.array(z.literal(true).or(z.object({
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    }))).optional(),
    (v) => v ? v.map((e) => `http-request disable-17-retry ${ifUnlessCondition(e === true ? undefined : e.condition)}`).join('\n') : '',
);

const httpRequestDoResolve = makeEntry(
    'http-request do-resolve',
    z.array(z.object({
        var: stringZod,
        resolvers: stringZod,
        ip: z.enum(['v4', 'v6']).optional(),
        expr: stringZod,
    })).optional(),
    (v) => v ? v.map((e) => {
        let out = `http-request do-resolve(${stringToString(e.var)},${stringToString(e.resolvers)}`;
        if (e.ip) out += `,${e.ip}`;
        out += `) ${stringToString(e.expr)}`;
        return out;
    }).join('\n') : '',
);

const httpRequestEarlyHint = makeEntry(
    'http-request early-hint',
    z.array(z.object({
        name: stringZod,
        fmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request early-hint ${stringToString(e.name)} ${stringToString(e.fmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestRedirect = makeEntry(
    'http-request redirect',
    z.array(z.object({
        rule: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request redirect ${stringToString(e.rule)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestReject = makeEntry(
    'http-request reject',
    z.array(z.literal(true).or(z.object({
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    }))).optional(),
    (v) => v ? v.map((e) => `http-request reject ${ifUnlessCondition(e === true ? undefined : e.condition)}`).join('\n') : '',
);

const httpRequestReplaceHeader = makeEntry(
    'http-request replace-header',
    z.array(z.object({
        name: stringZod,
        matchRegex: stringZod,
        replaceFmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request replace-header ${stringToString(e.name)} ${stringToString(e.matchRegex)} ${stringToString(e.replaceFmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestReplacePath = makeEntry(
    'http-request replace-path',
    z.array(z.object({
        matchRegex: stringZod,
        replaceFmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request replace-path ${stringToString(e.matchRegex)} ${stringToString(e.replaceFmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestReplaceUri = makeEntry(
    'http-request replace-uri',
    z.array(z.object({
        matchRegex: stringZod,
        replaceFmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request replace-uri ${stringToString(e.matchRegex)} ${stringToString(e.replaceFmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestReplaceValue = makeEntry(
    'http-request replace-value',
    z.array(z.object({
        name: stringZod,
        matchRegex: stringZod,
        replaceFmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request replace-value ${stringToString(e.name)} ${stringToString(e.matchRegex)} ${stringToString(e.replaceFmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestScIncGpc0 = makeEntry(
    'http-request sc-inc-gp0',
    z.array(z.object({
        scId: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request sc-inc-gpc0(${stringToString(e.scId)}) ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestScIncGpc1 = makeEntry(
    'http-request sc-inc-gp1',
    z.array(z.object({
        scId: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request sc-inc-gpc1(${stringToString(e.scId)}) ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestScSetGpt0 = makeEntry(
    'http-request sc-set-gpt0',
    z.array(z.object({
        scId: stringZod,
        int: z.number(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request sc-set-gpt0(${stringToString(e.scId)}) ${e.int} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetDst = makeEntry(
    'http-request set-dst',
    z.array(z.object({
        expr: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-dst ${stringToString(e.expr)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetDstPort = makeEntry(
    'http-request set-dst-port',
    z.array(z.object({
        expr: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-dst-port ${stringToString(e.expr)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetHeader = makeEntry(
    'http-request set-header',
    z.array(z.object({
        name: stringZod,
        fmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-header ${stringToString(e.name)} ${stringToString(e.fmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetLogLevel = makeEntry(
    'http-request set-log-level',
    z.array(z.object({
        level: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-log-level ${stringToString(e.level)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetMap = makeEntry(
    'http-request set-map',
    z.array(z.object({
        filename: stringZod,
        keyFmt: stringZod,
        valueFmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-map(${stringToString(e.filename)}) ${stringToString(e.keyFmt)} ${stringToString(e.valueFmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetMark = makeEntry(
    'http-request set-mark',
    z.array(z.object({
        mark: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-mark ${stringToString(e.mark)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetMethod = makeEntry(
    'http-request set-method',
    z.array(z.object({
        fmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-method ${stringToString(e.fmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetNice = makeEntry(
    'http-request set-nice',
    z.array(z.object({
        nice: z.number().min(-1024).max(1024),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-nice ${e.nice} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetPath = makeEntry(
    'http-request set-path',
    z.array(z.object({
        fmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-path ${stringToString(e.fmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetPriorityClass = makeEntry(
    'http-request set-priority-class',
    z.array(z.object({
        expr: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-priority-class ${stringToString(e.expr)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetPriorityOffset = makeEntry(
    'http-request set-priority-offset',
    z.array(z.object({
        expr: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-priority-offset ${stringToString(e.expr)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetQuery = makeEntry(
    'http-request set-query',
    z.array(z.object({
        expr: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-query ${stringToString(e.expr)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetSrc = makeEntry(
    'http-request set-src',
    z.array(z.object({
        expr: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-src ${stringToString(e.expr)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetSrcPort = makeEntry(
    'http-request set-src-port',
    z.array(z.object({
        expr: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-src-port ${stringToString(e.expr)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetTos = makeEntry(
    'http-request set-tos',
    z.array(z.object({
        tos: stringZod.or(z.number()),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-tos ${typeof (e.tos) === 'number' ? e.tos : stringToString(e.tos)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetUri = makeEntry(
    'http-request set-uri',
    z.array(z.object({
        fmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-uri ${stringToString(e.fmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSetVar = makeEntry(
    'http-request set-var',
    z.array(z.object({
        varName: stringZod,
        expr: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request set-var(${stringToString(e.varName)}) ${stringToString(e.expr)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSendSpoeGroup = makeEntry(
    'http-request send-spoe-group',
    z.array(z.object({
        engineName: stringZod,
        groupName: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request send-spoe-group ${stringToString(e.engineName)} ${stringToString(e.groupName)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestSilentDrop = makeEntry(
    'http-request silent-drop',
    z.array(z.literal(true).or(z.object({
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })),
    }))).optional(),
    (v) => v ? v.map((e) => `http-request silent-drop ${ifUnlessCondition(e === true ? undefined : e.condition)}`).join('\n') : '',
);

const httpRequestTarpit = makeEntry(
    'http-request tarpit',
    z.array(z.object({
        denyStatus: z.number().optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request tarpit ${e.denyStatus ? 'deny_status ' + e.denyStatus : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestTrackSc0 = makeEntry(
    'http-request track-sc0',
    z.array(z.object({
        key: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request track-sc0 ${stringToString(e.key)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestTrackSc1 = makeEntry(
    'http-request track-sc1',
    z.array(z.object({
        key: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request track-sc1 ${stringToString(e.key)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestTrackSc2 = makeEntry(
    'http-request track-sc2',
    z.array(z.object({
        key: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request track-sc2 ${stringToString(e.key)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestUnsetVar = makeEntry(
    'http-request unset-var',
    z.array(z.object({
        varName: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request unset-var(${stringToString(e.varName)}) ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestUseService = makeEntry(
    'http-request use-service',
    z.array(z.object({
        serviceName: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-request use-service ${stringToString(e.serviceName)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpRequestWaitForHandshake = makeEntry(
    'http-request wait-for-handshake',
    z.array(z.literal(true).or(z.object({
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })),
    }))).optional(),
    (v) => v ? v.map((e) => `http-request wait-for-handshake ${ifUnlessCondition(e === true ? undefined : e.condition)}`).join('\n') : '',
);


const httpResponse = makeEntry(
    "http-response",
    z.array(z.object({
        action: stringZod,
        options: z.array(stringZod),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response ${stringToString(e.action)} ${e.options.map(stringToString).join(' ')} ${ifUnlessCondition(e.condition)}`).join(' ') : '',
);

const httpResponseAddAcl = makeEntry(
    'http-response add-acl',
    z.array(z.object({
        filename: stringZod,
        keyfmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => {
        let out = `http-response add-acl(${stringToString(e.filename)}) ${stringToString(e.keyfmt)}`;
        if (e.condition) {
            if ('if' in e.condition) {
                out += ` if ${stringToString(e.condition.if)}`;
            } else {
                out += ` unless ${stringToString(e.condition.unless)}`;
            }
        }
        return out;
    }).join('\n') : '',
);

const httpResponseAddHeader = makeEntry(
    'http-response add-header',
    z.array(z.object({
        name: stringZod,
        fmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response add-header ${stringToString(e.name)} ${stringToString(e.fmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseAllow = makeEntry(
    'http-response allow',
    z.array(z.literal(true).or(z.object({
        if: stringZod,
    }).or(z.object({
        unless: stringZod,
    })))).optional(),
    (v) => v ? v.map((e) => e === true ? `http-response allow` : `http-response allow ${ifUnlessCondition(e)}`).join('\n') : '',
);

const httpResponseCacheStore = makeEntry(
    'http-response cache-store',
    z.array(z.object({
        name: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response cache-store ${stringToString(e.name)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseCapture = makeEntry(
    'http-response capture',
    z.array(z.object({
        sample: stringZod,
        id: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response capture ${stringToString(e.sample)} id ${stringToString(e.id)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseDelAcl = makeEntry(
    'http-response del-acl',
    z.array(z.object({
        filename: stringZod,
        keyfmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response del-acl(${stringToString(e.filename)}) ${stringToString(e.keyfmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseDelHeader = makeEntry(
    'http-response del-header',
    z.array(z.object({
        name: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response del-header ${stringToString(e.name)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseDelMap = makeEntry(
    'http-response del-map',
    z.array(z.object({
        filename: stringZod,
        keyfmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response del-map(${stringToString(e.filename)}) ${stringToString(e.keyfmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseDeny = makeEntry(
    'http-response deny',
    z.array(z.literal(true).or(z.object({
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    }))).optional(),
    (v) => v ? v.map((e) => e === true ? `http-response deny` : `http-response deny ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseRedirect = makeEntry(
    'http-response redirect',
    z.array(z.object({
        rule: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response redirect ${stringToString(e.rule)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseReplaceHeader = makeEntry(
    'http-response replace-header',
    z.array(z.object({
        name: stringZod,
        matchRegex: stringZod,
        replaceFmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response replace-header ${stringToString(e.name)} ${stringToString(e.matchRegex)} ${stringToString(e.replaceFmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseReplaceValue = makeEntry(
    'http-response replace-value',
    z.array(z.object({
        name: stringZod,
        matchRegex: stringZod,
        replaceFmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response replace-value ${stringToString(e.name)} ${stringToString(e.matchRegex)} ${stringToString(e.replaceFmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseScIncGpc0 = makeEntry(
    'http-response sc-inc-gp0',
    z.array(z.object({
        scId: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response sc-inc-gpc0(${stringToString(e.scId)}) ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseScIncGpc1 = makeEntry(
    'http-response sc-inc-gp1',
    z.array(z.object({
        scId: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response sc-inc-gpc1(${stringToString(e.scId)}) ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseScSetGpt0 = makeEntry(
    'http-response sc-set-gpt0',
    z.array(z.object({
        scId: stringZod,
        int: z.number(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response sc-set-gpt0(${stringToString(e.scId)}) ${e.int} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseSendSpoeGroup = makeEntry(
    'http-response send-spoe-group',
    z.array(z.object({
        engineName: stringZod,
        groupName: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response send-spoe-group ${stringToString(e.engineName)} ${stringToString(e.groupName)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseSetHeader = makeEntry(
    'http-response set-header',
    z.array(z.object({
        name: stringZod,
        fmt: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response set-header ${stringToString(e.name)} ${stringToString(e.fmt)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseSetLogLevel = makeEntry(
    'http-response set-log-level',
    z.array(z.object({
        level: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response set-log-level ${stringToString(e.level)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseSetMap = makeEntry(
    'http-response set-map',
    z.array(z.object({
        filename: stringZod,
        keyFmt: stringZod,
        valueFmt: stringZod,
    })).optional(),
    (v) => v ? v.map((e) => `http-response set-map(${stringToString(e.filename)}) ${stringToString(e.keyFmt)} ${stringToString(e.valueFmt)}`).join('\n') : '',
);

const httpResponseSetMark = makeEntry(
    'http-response set-mark',
    z.array(z.object({
        mark: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response set-mark ${stringToString(e.mark)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseSetNice = makeEntry(
    'http-response set-nice',
    z.array(z.object({
        nice: z.number().min(-1024).max(1024),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response set-nice ${e.nice} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseSetStatus = makeEntry(
    'http-response set-status',
    z.array(z.object({
        status: z.number(),
        reason: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response set-status ${e.status} ${e.reason ? 'reason ' + stringToString(e.reason) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseSetTos = makeEntry(
    'http-response set-tos',
    z.array(z.object({
        tos: stringZod.or(z.number()),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response set-tos ${typeof (e.tos) === 'number' ? e.tos : stringToString(e.tos)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseSetVar = makeEntry(
    'http-response set-var',
    z.array(z.object({
        varName: stringZod,
        expr: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response set-var(${stringToString(e.varName)}) ${stringToString(e.expr)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseSilentDrop = makeEntry(
    'http-response silent-drop',
    z.array(z.literal(true).or(z.object({
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })),
    }))).optional(),
    (v) => v ? v.map((e) => `http-response silent-drop ${ifUnlessCondition(e === true ? undefined : e.condition)}`).join('\n') : '',
);

const httpResponseTrackSc0 = makeEntry(
    'http-response track-sc0',
    z.array(z.object({
        key: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response track-sc0 ${stringToString(e.key)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseTrackSc1 = makeEntry(
    'http-response track-sc1',
    z.array(z.object({
        key: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response track-sc1 ${stringToString(e.key)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseTrackSc2 = makeEntry(
    'http-response track-sc2',
    z.array(z.object({
        key: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response track-sc2 ${stringToString(e.key)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpResponseUnsetVar = makeEntry(
    'http-response unset-var',
    z.array(z.object({
        varName: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `http-response unset-var(${stringToString(e.varName)}) ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const httpReuse = makeEntry(
    "http-reuse",
    z.array(z.enum(['never', 'safe', 'aggressive', 'always'])).optional(),
    (v) => v ? v.map((e) => `http-reuse ${e}`).join('\n') : '',
);

const httpSendNameHeader = makeEntry(
    "http-send-name-header",
    z.array(stringZod).optional(),
    (v) => v ? stringArrayPassthrough('http-send-name-header')(v) : '',
);

const id = makeEntry(
    "id",
    z.array(stringZod).optional(),
    (v) => v ? stringArrayPassthrough('id')(v) : '',
);

const ignorePersist = makeEntry(
    "ignore-persist",
    z.array(z.object({
        if: stringZod,
    }).or(z.object({
        unless: stringZod,
    }))).optional(),
    (v) => v ? v.map((e) => `ignore-persist ${ifUnlessCondition(e)}`).join('\n') : '',
);

const loadServerStateFromFile = makeEntry(
    "load-server-state-from-file",
    z.array(z.enum(['global', 'local', 'none'])).optional(),
    (v) => v ? v.map((e) => `load-server-state-from-file ${e}`).join('\n') : '',
);

const log = makeEntry(
    "log",
    z.array(z.literal('global').or(z.literal(false)).or(z.object({
        address: stringZod,
        length: z.number().optional(),
        format: z.enum(['rfc3164', 'rfc5424', 'short', 'raw']).optional(),
        sample: z.object({
            ranges: stringZod,
            smpSize: stringZod,
        }).optional(),
        facility: z.array(z.enum(['kern', 'user', 'mail', 'daemon', 'auth', 'syslog', 'lpr', 'news', 'uucp', 'cron', 'auth2', 'ftp', 'ntp', 'audit', 'alert', 'cron2', 'local0', 'local1', 'local2', 'local3', 'local4', 'local5', 'local6', 'local7'])),
        level: z.enum(['emerg', 'alert', 'crit', 'err', 'warning', 'notice', 'info', 'debug']).optional(),
        minLevel: z.enum(['emerg', 'alert', 'crit', 'err', 'warning', 'notice', 'info', 'debug']).optional(),
    }))).optional(),
    (v) => v ? v.map((e) => {
        if (e === 'global') return `log global`;
        if (e === false) return `no log`;

        let out = `log ${stringToString(e.address)}`;
        if (e.length) out += ` len ${e.length}`;
        if (e.format) out += ` format ${e.format}`;
        if (e.sample) out += ` sample ${e.sample.ranges}:${e.sample.smpSize}`;
        out += ` ${e.facility}`;
        if (e.level) out += ` ${e.level}`;
        if (e.minLevel) out += ` ${e.minLevel}`;
        return out;
    }).join('\n') : '',
);

const logFormat = makeEntry(
    "log-format",
    z.array(stringZod).optional(),
    (v) => v ? stringArrayPassthrough('log-format')(v) : '',
);

const logFormatSd = makeEntry(
    "log-format-sd",
    z.array(stringZod).optional(),
    (v) => v ? stringArrayPassthrough('log-format-sd')(v) : '',
);

const logTag = makeEntry(
    "log-tag",
    z.array(stringZod).optional(),
    (v) => v ? stringArrayPassthrough('log-tag')(v) : '',
);

const maxKeepAliveQueue = makeEntry(
    "max-keep-alive-queue",
    z.array(z.number()).optional(),
    (v) => v ? v.map((e) => `max-keep-alive-queue ${e}`).join('\n') : '',
);

const maxconn = makeEntry(
    "maxconn",
    z.array(z.number()).optional(),
    (v) => v ? v.map((e) => `maxconn ${e}`).join('\n') : '',
);

const mode = makeEntry(
    "mode",
    z.array(z.enum(['tcp', 'http', 'health'])).optional(),
    (v) => v ? v.map((e) => `mode ${e}`).join('\n') : '',
);

const monitorFail = makeEntry(
    "monitor fail",
    z.array(z.object({
        if: stringZod,
    }).or(z.object({
        unless: stringZod,
    }))).optional(),
    (v) => v ? v.map((e) => `monitor fail ${ifUnlessCondition(e)}`).join('\n') : '',
);

const monitorNet = makeEntry(
    "monitor-net",
    z.array(stringZod).optional(),
    (v) => v ? stringArrayPassthrough('monitor-net')(v) : '',
);

const monitorUri = makeEntry(
    "monitor-uri",
    z.array(stringZod).optional(),
    (v) => v ? stringArrayPassthrough('monitor-uri')(v) : '',
);

const optionAbortonclose = makeEntry(
    "option abortonclose",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option abortonclose' : 'no option abortonclose'),
);

const optionAcceptInvalidHttpRequest = makeEntry(
    "option accept-invalid-http-request",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option accept-invalid-http-request' : 'no option accept-invalid-http-request'),
);

const optionAcceptInvalidHttpResponse = makeEntry(
    "option accept-invalid-http-response",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option accept-invalid-http-response' : 'no option accept-invalid-http-response'),
);

const optionAllbackups = makeEntry(
    "option allbackups",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option allbackups' : 'no option allbackups'),
);

const optionCheckcache = makeEntry(
    "option checkcache",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option checkcache' : 'no option checkcache'),
);

const optionClitcpka = makeEntry(
    "option clitcpka",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option clitcpka' : 'no option clitcpka'),
);

const optionContstats = makeEntry(
    "option contstats",
    z.literal(true).optional(),
    (v) => v === undefined ? '' : 'option contstats',
);

const optionDisableH2Upgrade = makeEntry(
    "option disable-h2-upgrade",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option disable-h2-upgrade' : 'no option disable-h2-upgrade'),
);

const optionDontlogNormal = makeEntry(
    "option dontlog-normal",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option dontlog-normal' : 'no option dontlog-normal'),
);

const optionDontlognull = makeEntry(
    "option dontlognull",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option dontlognull' : 'no option dontlognull'),
);

const optionForwardfor = makeEntry(
    "option forwardfor",
    z.array(z.literal(true).or(z.object({
        except: stringZod.optional(),
        header: stringZod.optional(),
        ifNone: z.boolean().optional(),
    }))).optional(),
    (v) => v ? v.map((e) => {
        let out = `option forwardfor`;
        if (e === true) return out;
        if (e.except) out += ` except ${stringToString(e.except)}`;
        if (e.header) out += ` header ${stringToString(e.header)}`;
        if (e.ifNone) out += ` if-none`;
        return out;
    }).join('\n') : '',
);

const optionH1CaseAdjustBogusClient = makeEntry(
    "option h1-case-adjust-bogus-client",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option h1-case-adjust-bogus-client' : 'no option h1-case-adjust-bogus-client'),
);

const optionH1CaseAdjustBogusServer = makeEntry(
    "option h1-case-adjust-bogus-server",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option h1-case-adjust-bogus-server' : 'no option h1-case-adjust-bogus-server'),
);

const optionHttpBufferRequest = makeEntry(
    "option http-buffer-request",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http-buffer-request' : 'no option http-buffer-request'),
);

const optionHttpIgnoreProbes = makeEntry(
    "option http-ignore-probes",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http-ignore-probes' : 'no option http-ignore-probes'),
);

const optionHttpKeepAlive = makeEntry(
    "option http-keep-alive",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http-keep-alive' : 'no option http-keep-alive'),
);

const optionHttpNoDelay = makeEntry(
    "option http-no-delay",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http-no-delay' : 'no option http-no-delay'),
);

const optionHttpPretendKeepalive = makeEntry(
    "option http-pretend-keepalive",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http-pretend-keepalive' : 'no option http-pretend-keepalive'),
);

const optionHttpServerClose = makeEntry(
    "option http-server-close",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http-server-close' : 'no option http-server-close'),
);

const optionHttpTunnel = makeEntry(
    "option http-tunnel",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http-tunnel' : 'no option http-tunnel'),
);

const optionHttpUseProxyHeader = makeEntry(
    "option http-use-proxy-header",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http-use-proxy-header' : 'no option http-use-proxy-header'),
);

const optionHttpUseHtx = makeEntry(
    "option http-use-htx",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http-use-htx' : 'no option http-use-htx'),
);

const optionHttpchk = makeEntry(
    "option httpchk",
    z.array(
        z.literal(true)
            .or(z.object({
                uri: stringZod,
            }))
            .or(z.object({
                method: stringZod,
                uri: stringZod,
                version: stringZod.optional(),
            }))
    ).optional(),
    (v) => v ? v.map((e) => {
        let out = `option httpchk`;
        if (e === true) return;
        if ('method' in e) {
            out += ` ${stringToString(e.method)} ${stringToString(e.uri)}`;
            if (e.version) out += ` ${stringToString(e.version)}`;
        } else {
            out += ` ${stringToString(e.uri)}`;
        }

        return out;
    }).join('\n') : '',
);

const optionHttpclose = makeEntry(
    "option httpclose",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option httpclose' : 'no option httpclose'),
);

const optionHttplog = makeEntry(
    "option httplog",
    z.literal(true).or(z.object({
        clf: stringZod,
    })).optional(),
    (v) => {
        if (v === undefined) return '';
        if (v === true) return `option httplog`;
        return `option httplog ${v.clf}`
    },
);

const optionHttpProxy = makeEntry(
    "option http_proxy",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option http_proxy' : 'no option http_proxy'),
);

const optionIndependentStreams = makeEntry(
    "option independent-streams",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option independent-streams' : 'no option independent-streams'),
);

const optionLdapCheck = makeEntry(
    "option ldap-check",
    z.boolean().optional(),
    (v) => v ? `optional ldap-check` : '',
);

const optionExternalCheck = makeEntry(
    "option external-check",
    z.boolean().optional(),
    (v) => v ? `optional external-check` : '',
);

const optionLogHealthChecks = makeEntry(
    "option log-health-checks",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option log-health-checks' : 'no option log-health-checks'),
);

const optionLogSeparateErrors = makeEntry(
    "option log-separate-errors",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option log-separate-errors' : 'no option log-separate-errors'),
);

const optionLogasap = makeEntry(
    "option logasap",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option logasap' : 'no option logasap'),
);

const optionMysqlCheck = makeEntry(
    "option mysql-check",
    z.literal(true).or(z.object({
        user: stringZod,
        post41: z.boolean().optional(),
    })).optional(),
    (v) => {
        if (v === undefined) return '';
        if (v === true) return `option mysql-check`;
        return `option mysql-check user ${stringToString(v.user)} ${v.post41 ? 'post-41' : ''}`
    },
);

const optionNolinger = makeEntry(
    "option nolinger",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option nolinger' : 'no option nolinger'),
);

const optionOriginalto = makeEntry(
    "option originalto",
    z.literal(true).or(z.object({
        except: stringZod.optional(),
        header: stringZod.optional(),
    })).optional(),
    (v) => {
        if (v === undefined) return '';
        if (v === true) return 'option originalto';
        return `option originalto ${v.except ? 'except ' + stringToString(v.except) : ''} ${v.header ? 'header ' + stringToString(v.header) : ''}`;
    },
);

const optionPersist = makeEntry(
    "option persist",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option persist' : 'no option persist'),
);

const optionPgsqlCheck = makeEntry(
    "option pgsql-check",
    z.literal(true).or(z.object({
        user: stringZod,
    })).optional(),
    (v) => {
        if (v === undefined) return '';
        if (v === true) return 'option pgsql-check';
        return `option pgsql-check user ${stringToString(v.user)}`;
    },
);

const optionPreferLastServer = makeEntry(
    "option prefer-last-server",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option prefer-last-server' : 'no option prefer-last-server'),
);

const optionRedispatch = makeEntry(
    "option redispatch",
    z.boolean().or(timeZod).optional(),
    (v) => {
        if (v === undefined) return '';
        if (v === true) return 'option redispatch';
        if (v === false) return 'no option redispatch';
        return `option redispatch ${v}`;
    },
);

const optionRedisCheck = makeEntry(
    "option redis-check",
    z.literal(true).optional(),
    (v) => v ? 'option redis-check' : '',
);

const optionSmtpchk = makeEntry(
    "option smtpchk",
    z.literal(true).or(z.object({
        hello: stringZod,
        domain: stringZod,
    })).optional(),
    (v) => {
        if (v === undefined) return '';
        if (v === true) return 'option smtpchk';
        return `option smtpchk ${stringToString(v.hello)} ${stringToString(v.domain)}`;
    },
);

const optionSocketStats = makeEntry(
    "option socket-stats",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option socket-stats' : 'no option socket-stats'),
);

const optionSpliceAuto = makeEntry(
    "option splice-auto",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option splice-auto' : 'no option splice-auto'),
);

const optionSpliceRequest = makeEntry(
    "option splice-request",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option splice-request' : 'no option splice-request'),
);

const optionSpliceResponse = makeEntry(
    "option splice-response",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option splice-response' : 'no option splice-response'),
);

const optionSpopCheck = makeEntry(
    "option spop-check",
    z.literal(true).optional(),
    (v) => v ? 'option spop-check' : '',
);

const optionSrvtcpka = makeEntry(
    "option srvtcpka",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option srvtcpka' : 'no option srvtcpka'),
);

const optionSslHelloChk = makeEntry(
    "option ssl-hello-chk",
    z.literal(true).optional(),
    (v) => v ? 'option ssl-hello-chk' : '',
);

const optionTcpCheck = makeEntry(
    "option tcp-check",
    z.literal(true).optional(),
    (v) => v ? 'option tcp-check' : '',
);

const optionTcpSmartAccept = makeEntry(
    "option tcp-smart-accept",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option tcp-smart-accept' : 'no option tcp-smart-accept'),
);

const optionTcpSmartConnect = makeEntry(
    "option tcp-smart-connect",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option tcp-smart-connect' : 'no option tcp-smart-connect'),
);

const optionTcpka = makeEntry(
    "option tcpka",
    z.literal(true).optional(),
    (v) => v ? 'option tcpka' : '',
);

const optionTcplog = makeEntry(
    "option tcplog",
    z.literal(true).optional(),
    (v) => v ? 'option tcplog' : '',
);

const optionTransparent = makeEntry(
    "option transparent",
    z.boolean().optional(),
    (v) => v === undefined ? '' : (v ? 'option transparent' : 'no option transparent'),
);

const externalCheckCommand = makeEntry(
    "external-check command",
    z.array(stringZod).optional(),
    (v) => v ? stringArrayPassthrough('external-check command')(v) : '',
);

const externalCheckPath = makeEntry(
    "external-check path",
    z.array(stringZod).optional(),
    (v) => v ? stringArrayPassthrough('external-check path')(v) : '',
);

const persistRdpCookie = makeEntry(
    "persist rdp-cookie",
    z.array(z.literal(true).or(stringZod)).optional(),
    (v) => v ? v.map((e) => e === true ? 'persist rdp-cookie' : `persist rdp-cookie ${stringToString(e)}`).join('\n') : '',
);

const rateLimitSessions = makeEntry(
    "rate-limit sessions",
    z.array(z.number()).optional(),
    (v) => v ? v.map((e) => `rate-limit sessions ${e}`).join('\n') : '',
);

const redirect = makeEntry(
    "redirect",
    z.array(
        z.object({
            mode: z.enum(['location', 'prefix', 'scheme']),
            value: stringZod,
            code: z.number().optional(),
            option: stringZod.optional(),
            condition: z.object({
                if: stringZod,
            }).or(z.object({
                unless: stringZod,
            })).optional(),
        }),
    ).optional(),
    (v) => v ? v.map((e) => {
        let out = `redirect ${e.mode} ${stringToString(e.value)}`;
        if (e.code) out += ` code ${e.code}`;
        if(e.option) out += ` ${stringToString(e.option)}`;
        out += ` ${ifUnlessCondition(e.condition)}`;
        return out;
    }).join('\n') : '',
);

const redisp = makeEntry(
    "redisp",
    z.boolean().optional(),
    (v) => v === true ? 'redisp' : '',
);

const redispatch = makeEntry(
    "redispatch",
    z.boolean().optional(),
    (v) => v === true ? 'redispatch' : '',
);

const reqadd = makeEntry(
    "reqadd",
    z.array(z.object({
        string: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `reqadd ${stringToString(e.string)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const reqallow = makeEntry(
    "reqallow",
    z.array(z.object({
        search: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        ignoreCase: z.boolean().optional(),
    })).optional(),
    (v) => v ? v.map((e) => `req${e.ignoreCase ? 'i' : ''}allow ${stringToString(e.search)} ${ifUnlessCondition(e.condition)}`).join('') : '',
);

const reqdel = makeEntry(
    "reqdel",
    z.array(z.object({
        search: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        ignoreCase: z.boolean().optional(),
    })).optional(),
    (v) => v ? v.map((e) => `req${e.ignoreCase ? 'i' : ''}del ${stringToString(e.search)} ${ifUnlessCondition(e.condition)}`).join('') : '',
);

const reqdeny = makeEntry(
    "reqdeny",
    z.array(z.object({
        search: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        ignoreCase: z.boolean().optional(),
    })).optional(),
    (v) => v ? v.map((e) => `req${e.ignoreCase ? 'i' : ''}deny ${stringToString(e.search)} ${ifUnlessCondition(e.condition)}`).join('') : '',
);

const reqpass = makeEntry(
    "reqpass",
    z.array(z.object({
        search: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        ignoreCase: z.boolean().optional(),
    })).optional(),
    (v) => v ? v.map((e) => `req${e.ignoreCase ? 'i' : ''}pass ${stringToString(e.search)} ${ifUnlessCondition(e.condition)}`).join('') : '',
);

const reqrep = makeEntry(
    "reqrep",
    z.array(z.object({
        search: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        ignoreCase: z.boolean().optional(),
    })).optional(),
    (v) => v ? v.map((e) => `req${e.ignoreCase ? 'i' : ''}rep ${stringToString(e.search)} ${ifUnlessCondition(e.condition)}`).join('') : '',
);

const reqtarpit = makeEntry(
    "reqtarpit",
    z.array(z.object({
        search: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        ignoreCase: z.boolean().optional(),
    })).optional(),
    (v) => v ? v.map((e) => `req${e.ignoreCase ? 'i' : ''}tarpit ${stringToString(e.search)} ${ifUnlessCondition(e.condition)}`).join('') : '',
);

const retries = makeEntry(
    "retries",
    z.number().optional(),
    (v) => v ? `retries ${v}` : '',
);

const retryOn = makeEntry(
    "retry-on",
    z.array(z.enum(['none', 'conn-failure', 'empty-response', 'junk-response', 'response-timeout', '0rtt-rejected', 'all-retryable-errors']).or(z.number())).optional(),
    (v) => v ? `retry-on ${v.join(' ')}` : '',
);

const rspadd = makeEntry(
    "rspadd",
    z.array(z.object({
        string: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `rspadd ${stringToString(e.string)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const rspdel = makeEntry(
    "rspdel",
    z.array(z.object({
        search: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        ignoreCase: z.boolean().optional(),
    })).optional(),
    (v) => v ? v.map((e) => `rsp${e.ignoreCase ? 'i' : ''}del ${stringToString(e.search)} ${ifUnlessCondition(e.condition)}`).join('') : '',
);

const rspdeny = makeEntry(
    "rspdeny",
    z.array(z.object({
        search: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        ignoreCase: z.boolean().optional(),
    })).optional(),
    (v) => v ? v.map((e) => `rsp${e.ignoreCase ? 'i' : ''}deny ${stringToString(e.search)} ${ifUnlessCondition(e.condition)}`).join('') : '',
);

const rsprep = makeEntry(
    "rsprep",
    z.array(z.object({
        search: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
        ignoreCase: z.boolean().optional(),
    })).optional(),
    (v) => v ? v.map((e) => `rsp${e.ignoreCase ? 'i' : ''}rep ${stringToString(e.search)} ${ifUnlessCondition(e.condition)}`).join('') : '',
);

const server = makeEntry(
    "server",
    z.array(z.object({
        name: stringZod,
        address: stringZod,
        port: z.number().optional(),
        params: z.array(stringZod).optional()
    })).optional(),
    (v) => v ? v.map((e) => `server ${stringToString(e.name)} ${stringToString(e.address)}${e.port ? ':' + e.port : ''} ${e.params ? e.params.map(stringToString).join(' ') : ''}`).join('\n') : '',
);

const serverStateFileName = makeEntry(
    "server-state-file-name",
    z.array(z.literal(true).or(z.literal('use-backend-name')).or(stringZod)).optional(),
    (v) => v ? v.map((e) => e === true ? `use-backend-name` : `use-backend-name ${stringToString(e)}`).join('\n') : '',
);

const serverTemplate = makeEntry(
    "server-template",
    z.array(z.object({
        prefix: stringZod,
        nr: z.number().or(z.object({
            min: z.number(),
            max: z.number(),
        })),
        fqdn: stringZod,
        port: z.number().optional(),
        params: z.array(stringZod).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `server-template ${stringToString(e.prefix)} ${typeof (e.nr) === 'number' ? e.nr : `${e.nr.min}-${e.nr.max}`} ${stringToString(e.fqdn)}${e.port ? ':' + e.port : ''} ${e.params ? e.params.map(stringToString).join(' ') : ''}`).join('\n') : '',
);

const source = makeEntry(
    "source",
    z.array(z.object({
        addr: stringZod,
        port: z.number().optional(),
        mode: z.object({
            usesrc: z.enum(['client', 'clientip']).or(z.object({
                addr: stringZod,
                port: z.number().optional(),
            })).or(z.object({
                hdr: stringZod,
                occ: stringZod.optional(),
            }))
        }).or(z.object({
            interface: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => {
        let out = `source ${stringToString(e.addr)}${e.port ? ':' + e.port : ''}`;
        if ('mode' in e && e.mode) {
            if ('usesrc' in e.mode) {
                if (typeof (e.mode.usesrc) === 'string') {
                    out += ` usesrc ${e.mode.usesrc}`;
                } else if ('addr' in e.mode.usesrc) {
                    out += ` usesrc ${stringToString(e.mode.usesrc.addr)}${e.mode.usesrc.port ? ':' + e.mode.usesrc.port : ''}`;
                } else {
                    out += ` usesrc hdr_ip(${stringToString(e.mode.usesrc.hdr)}${e.mode.usesrc.occ ? ',' + stringToString(e.mode.usesrc.occ) : ''})`;
                }
            } else {
                out += ` interface ${stringToString(e.mode.interface)}`;
            }
        }
        return out;
    }).join('\n') : '',
);

const srvtimeout = makeEntry(
    "srvtimeout",
    timeZod.optional(),
    (v) => v ? passthrough('srvtimeout')(v) : '',
);

const statsAdmin = makeEntry(
    "stats admin",
    z.array(z.object({
        if: stringZod,
    }).or(z.object({
        unless: stringZod,
    }))).optional(),
    (v) => v ? v.map((e) => `stats admin ${ifUnlessCondition(e)}`).join('\n') : '',
);

const statsAuth = makeEntry(
    "stats auth",
    z.array(z.object({
        user: stringZod,
        passwd: stringZod,
    })).optional(),
    (v) => v ? v.map((e) => `stats auth ${stringToString(e.user)}:${stringToString(e.passwd)}`).join('\n') : '',
);

const statsEnable = makeEntry(
    "stats enable",
    z.boolean().optional(),
    (v) => v ? 'stats enable' : '',
);

const statsHideVersion = makeEntry(
    "stats hide-version",
    z.boolean().optional(),
    (v) => v ? 'stats hide-version' : '',
);

const statsHttpRequest = makeEntry(
    "stats http-request",
    z.array(z.object({
        mode: z.enum(['allow', 'deny', 'auth']).or(z.object({
            authRealm: stringZod,
        })),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => {
        if (typeof (e.mode) === 'string') return `stats http-request ${e.mode} ${ifUnlessCondition(e.condition)}`;
        return `stats http-request auth realm ${stringToString(e.mode.authRealm)} ${ifUnlessCondition(e.condition)}`;
    }).join('\n') : '',
);

const statsRealm = makeEntry(
    "stats realm",
    z.array(stringZod).optional(),
    (v) => v ? v.map((e) => `stats realm ${stringToString(e)}`).join('\n') : '',
);

const statsRefresh = makeEntry(
    "stats refresh",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `stats refresh ${e}`).join('\n') : '',
);

const statsScope = makeEntry(
    "stats scope",
    z.array(z.literal(".").or(stringZod)).optional(),
    (v) => v ? v.map((e) => `stats scope ${stringToString(e)}`).join('\n') : '',
);

const statsShowDesc = makeEntry(
    "stats show-desc",
    z.literal(true).or(stringZod).optional(),
    (v) => v === true ? `stats show-desc` : (v === undefined ? '' : `stats show-desc ${stringToString(v)}`),
);

const statsShowLegends = makeEntry(
    "stats show-legends",
    z.boolean().optional(),
    (v) => v ? 'stats show-legends' : '',
);

const statsShowNode = makeEntry(
    "stats show-node",
    z.literal(true).or(stringZod).optional(),
    (v) => v === true ? `stats show-node` : (v === undefined ? '' : `stats show-node ${stringToString(v)}`),
);

const statsUri = makeEntry(
    "stats uri",
    stringZod.optional(),
    (v) => v ? `stats uri ${stringToString(v)}` : '',
);

const stickMatch = makeEntry(
    "stick match",
    z.array(z.object({
        pattern: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `stick match ${stringToString(e.pattern)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const stickOn = makeEntry(
    "stick on",
    z.array(z.object({
        pattern: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `stick on ${stringToString(e.pattern)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const stickStoreRequest = makeEntry(
    "stick store-request",
    z.array(z.object({
        pattern: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `stick store-request ${stringToString(e.pattern)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const stickStoreResponse = makeEntry(
    "stick store-response",
    z.array(z.object({
        pattern: stringZod,
        table: stringZod.optional(),
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `stick store-response ${stringToString(e.pattern)} ${e.table ? 'table ' + stringToString(e.table) : ''} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const stickTable = makeEntry(
    "stick-table",
    z.array(z.object({
        type: z.enum(['ip', 'integer']).or(z.object({
            mode: z.enum(['string', 'binary']),
            length: z.number().optional()
        })),
        size: sizeZod,
        expire: timeZod.optional(),
        nopurge: z.boolean().optional(),
        peers: stringZod.optional(),
        store: z.array(stringZod).optional(),
    })).optional(),
    (v) => v ? v.map((e) => {
        let out = `stick-table type`;
        if (typeof (e.type) === 'string') out += ` ${e.type}`;
        else out += ` ${e.type.mode}${e.type.length !== undefined ? ' len ' + e.type.length : ''}`;
        out += ` size ${e.size}`;
        if (e.expire) out += ` expire ${e.expire}`;
        if (e.nopurge) out += ` nopurge`;
        if (e.peers) out += ` peers ${stringToString(e.peers)}`;
        if (e.store) out += ` ${e.store.map((e) => 'store ' + stringToString(e)).join(' ')}`;
        return out;
    }).join('\n') : '',
);

const tcpCheckConnect = makeEntry(
    "tcp-check connect",
    z.array(z.literal(true).or(z.array(stringZod))).optional(),
    (v) => v ? v.map((e) => e === true ? 'tcp-check connect' : `tcp-check connect ${e.map(stringToString).join(' ')}`).join('\n') : '',
);

const tcpCheckExpect = makeEntry(
    "tcp-check expect",
    z.array(z.object({
        invert: z.boolean().optional(),
        match: stringZod,
        pattern: stringZod,
    })).optional(),
    (v) => v ? v.map((e) => `tcp-check expect${e.invert ? ' !' : ''} ${stringToString(e.match)} ${stringToString(e.pattern)}`).join('\n') : '',
);

const tcpCheckSend = makeEntry(
    "tcp-check send",
    z.array(stringZod).optional(),
    (v) => v ? v.map((e) => `tcp-check send ${stringToString(e)}`).join('\n') : '',
);

const tcpCheckSendBinary = makeEntry(
    "tcp-check send-binary",
    z.array(stringZod).optional(),
    (v) => v ? v.map((e) => `tcp-check send-binary ${stringToString(e)}`).join('\n') : '',
);

const tcpRequestConnection = makeEntry(
    "tcp-request connection",
    z.array(z.object({
        action: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `tcp-request connection ${stringToString(e.action)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const tcpRequestContent = makeEntry(
    "tcp-request content",
    z.array(z.object({
        action: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `tcp-request content ${stringToString(e.action)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const tcpRequestInspectDelay = makeEntry(
    "tcp-request inspect-delay",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `tcp-request inspect-delay ${e}`).join('\n') : '',
);

const tcpRequestSession = makeEntry(
    "tcp-request session",
    z.array(z.object({
        action: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `tcp-request session ${stringToString(e.action)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const tcpResponseContent = makeEntry(
    "tcp-response content",
    z.array(z.object({
        action: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional()
    })).optional(),
    (v) => v ? v.map((e) => `tcp-response content ${stringToString(e.action)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);

const tcpResponseInspectDelay = makeEntry(
    "tcp-response inspect-delay",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `tcp-response inspect-delay ${e}`).join('\n') : '',
);

const timeoutCheck = makeEntry(
    "timeout check",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout check ${e}`).join('\n') : '',
);

const timeoutClient = makeEntry(
    "timeout client",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout client ${e}`).join('\n') : '',
);

const timeoutClientFin = makeEntry(
    "timeout client-fin",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout client-fin ${e}`).join('\n') : '',
);

const timeoutClitimeout = makeEntry(
    "timeout clitimeout",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout clitimeout ${e}`).join('\n') : '',
);

const timeoutConnect = makeEntry(
    "timeout connect",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout connect ${e}`).join('\n') : '',
);

const timeoutContimeout = makeEntry(
    "timeout contimeout",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout contimeout ${e}`).join('\n') : '',
);

const timeoutHttpKeepAlive = makeEntry(
    "timeout http-keep-alive",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout http-keep-alive ${e}`).join('\n') : '',
);

const timeoutHttpRequest = makeEntry(
    "timeout http-request",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout http-request ${e}`).join('\n') : '',
);

const timeoutQueue = makeEntry(
    "timeout queue",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout queue ${e}`).join('\n') : '',
);

const timeoutServer = makeEntry(
    "timeout server",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout server ${e}`).join('\n') : '',
);

const timeoutServerFin = makeEntry(
    "timeout server-fin",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout server-fin ${e}`).join('\n') : '',
);

const timeoutSrvtimeout = makeEntry(
    "timeout srvtimeout",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout srvtimeout ${e}`).join('\n') : '',
);

const timeoutTarpit = makeEntry(
    "timeout tarpit",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout tarpit ${e}`).join('\n') : '',
);

const timeoutTunnel = makeEntry(
    "timeout tunnel",
    z.array(timeZod).optional(),
    (v) => v ? v.map((e) => `timeout tunnel ${e}`).join('\n') : '',
);

const transparent = makeEntry(
    "transparent",
    z.boolean().optional(),
    (v) => v ? 'transparent' : '',
);

const uniqueIdFormat = makeEntry(
    "unique-id-format",
    stringZod.optional(),
    (v) => v ? passthrough('unique-id-format')(v) : '',
);

const uniqueIdHeader = makeEntry(
    "unique-id-header",
    stringZod.optional(),
    (v) => v ? passthrough('unique-id-header')(v) : '',
);

const useBackend = makeEntry(
    "use_backend",
    z.array(stringZod.or(z.object({
        backend: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        }))
    }))).optional(),
    (v) => v ? v.map((e) => typeof (e) === 'object' && 'backend' in e ? `use_backend ${stringToString(e.backend)} ${ifUnlessCondition(e.condition)}` : `use_backend ${stringToString(e)}`).join('\n') : '',
);

const useServer = makeEntry(
    "use-server",
    z.array(z.object({
        server: stringZod,
        condition: z.object({
            if: stringZod,
        }).or(z.object({
            unless: stringZod,
        })).optional(),
    })).optional(),
    (v) => v ? v.map((e) => `use-server ${stringToString(e.server)} ${ifUnlessCondition(e.condition)}`).join('\n') : '',
);


const defaults = [
    backlog,
    balance,
    bindProcess,
    clitimeout,
    compression,
    contimeout,
    cookie,
    defaultServer,
    defaultBackend,
    disabled,
    dynamicCookieKey,
    emailAlertFrom,
    emailAlertLevel,
    emailAlertMailers,
    emailAlertMyhostname,
    emailAlertTo,
    enabled,
    errorfile,
    errorloc,
    errorloc302,
    errorloc303,
    fullconn,
    grace,
    hashBalanceFactor,
    hashType,
    httpCheckDisableOn404,
    httpCheckSend,
    httpCheckSendState,
    httpReuse,
    httpSendNameHeader,
    loadServerStateFromFile,
    log,
    logFormat,
    logFormatSd,
    logTag,
    maxKeepAliveQueue,
    maxconn,
    mode,
    monitorNet,
    monitorUri,
    optionAbortonclose,
    optionAcceptInvalidHttpRequest,
    optionAcceptInvalidHttpResponse,
    optionAllbackups,
    optionCheckcache,
    optionClitcpka,
    optionContstats,
    optionDisableH2Upgrade,
    optionDontlogNormal,
    optionDontlognull,
    optionForwardfor,
    optionH1CaseAdjustBogusClient,
    optionH1CaseAdjustBogusServer,
    optionHttpBufferRequest,
    optionHttpIgnoreProbes,
    optionHttpKeepAlive,
    optionHttpNoDelay,
    optionHttpPretendKeepalive,
    optionHttpServerClose,
    optionHttpTunnel,
    optionHttpUseProxyHeader,
    optionHttpUseHtx,
    optionHttpchk,
    optionHttpclose,
    optionHttplog,
    optionHttpProxy,
    optionIndependentStreams,
    optionLdapCheck,
    optionExternalCheck,
    optionLogHealthChecks,
    optionLogSeparateErrors,
    optionLogasap,
    optionMysqlCheck,
    optionNolinger,
    optionOriginalto,
    optionPersist,
    optionPgsqlCheck,
    optionPreferLastServer,
    optionRedispatch,
    optionRedisCheck,
    optionSmtpchk,
    optionSocketStats,
    optionSpliceAuto,
    optionSpliceRequest,
    optionSpliceResponse,
    optionSpopCheck,
    optionSrvtcpka,
    optionSslHelloChk,
    optionTcpCheck,
    optionTcpSmartAccept,
    optionTcpSmartConnect,
    optionTcpka,
    optionTcplog,
    optionTransparent,
    externalCheckCommand,
    externalCheckPath,
    persistRdpCookie,
    rateLimitSessions,
    redisp,
    redispatch,
    retries,
    retryOn,
    serverStateFileName,
    source,
    srvtimeout,
    statsAuth,
    statsEnable,
    statsHideVersion,
    statsRealm,
    statsRefresh,
    statsScope,
    statsShowDesc,
    statsShowLegends,
    statsShowNode,
    statsUri,
    timeoutCheck,
    timeoutClient,
    timeoutClientFin,
    timeoutClitimeout,
    timeoutConnect,
    timeoutContimeout,
    timeoutHttpKeepAlive,
    timeoutHttpRequest,
    timeoutQueue,
    timeoutServer,
    timeoutServerFin,
    timeoutSrvtimeout,
    timeoutTarpit,
    timeoutTunnel,
    transparent,
    uniqueIdFormat,
    uniqueIdHeader,
];
const frontend = [
    acl,
    backlog,
    bind,
    bindProcess,
    block,
    captureCookie,
    captureRequestHeader,
    captureResponseHeader,
    clitimeout,
    compression,
    declareCapture,
    defaultBackend,
    description,
    disabled,
    emailAlertFrom,
    emailAlertLevel,
    emailAlertMailers,
    emailAlertMyhostname,
    emailAlertTo,
    enabled,
    errorfile,
    errorloc,
    errorloc302,
    errorloc303,
    filter,
    grace,
    httpRequest,
    httpRequestAddAcl,
    httpRequestAddHeader,
    httpRequestAllow,
    httpRequestAuth,
    httpRequestCacheUse,
    httpRequestCapture,
    httpRequestDelAcl,
    httpRequestDelHeader,
    httpRequestDelMap,
    httpRequestDeny,
    httpRequestDisable17Retry,
    httpRequestDoResolve,
    httpRequestEarlyHint,
    httpRequestRedirect,
    httpRequestReject,
    httpRequestReplaceHeader,
    httpRequestReplacePath,
    httpRequestReplaceUri,
    httpRequestReplaceValue,
    httpRequestScIncGpc0,
    httpRequestScIncGpc1,
    httpRequestScSetGpt0,
    httpRequestSetDst,
    httpRequestSetDstPort,
    httpRequestSetHeader,
    httpRequestSetLogLevel,
    httpRequestSetMap,
    httpRequestSetMark,
    httpRequestSetMethod,
    httpRequestSetNice,
    httpRequestSetPath,
    httpRequestSetPriorityClass,
    httpRequestSetPriorityOffset,
    httpRequestSetQuery,
    httpRequestSetSrc,
    httpRequestSetSrcPort,
    httpRequestSetTos,
    httpRequestSetUri,
    httpRequestSetVar,
    httpRequestSendSpoeGroup,
    httpRequestSilentDrop,
    httpRequestTarpit,
    httpRequestTrackSc0,
    httpRequestTrackSc1,
    httpRequestTrackSc2,
    httpRequestUnsetVar,
    httpRequestUseService,
    httpRequestWaitForHandshake,
    httpResponse,
    httpResponseAddAcl,
    httpResponseAddHeader,
    httpResponseAllow,
    httpResponseCacheStore,
    httpResponseCapture,
    httpResponseDelAcl,
    httpResponseDelHeader,
    httpResponseDelMap,
    httpResponseDeny,
    httpResponseRedirect,
    httpResponseReplaceHeader,
    httpResponseReplaceValue,
    httpResponseScIncGpc0,
    httpResponseScIncGpc1,
    httpResponseScSetGpt0,
    httpResponseSendSpoeGroup,
    httpResponseSetHeader,
    httpResponseSetLogLevel,
    httpResponseSetMap,
    httpResponseSetMark,
    httpResponseSetNice,
    httpResponseSetStatus,
    httpResponseSetTos,
    httpResponseSetVar,
    httpResponseSilentDrop,
    httpResponseTrackSc0,
    httpResponseTrackSc1,
    httpResponseTrackSc2,
    httpResponseUnsetVar,
    id,
    log,
    logFormat,
    logFormatSd,
    logTag,
    maxconn,
    mode,
    monitorFail,
    monitorNet,
    monitorUri,
    optionAcceptInvalidHttpRequest,
    optionClitcpka,
    optionContstats,
    optionDisableH2Upgrade,
    optionDontlogNormal,
    optionDontlognull,
    optionForwardfor,
    optionH1CaseAdjustBogusClient,
    optionHttpBufferRequest,
    optionHttpIgnoreProbes,
    optionHttpKeepAlive,
    optionHttpNoDelay,
    optionHttpServerClose,
    optionHttpTunnel,
    optionHttpUseProxyHeader,
    optionHttpUseHtx,
    optionHttpclose,
    optionHttplog,
    optionHttpProxy,
    optionIndependentStreams,
    optionLogSeparateErrors,
    optionLogasap,
    optionNolinger,
    optionOriginalto,
    optionSocketStats,
    optionSpliceAuto,
    optionSpliceRequest,
    optionSpliceResponse,
    optionTcpSmartAccept,
    optionTcpka,
    optionTcplog,
    rateLimitSessions,
    redirect,
    reqadd,
    reqallow,
    reqdel,
    reqdeny,
    reqpass,
    reqrep,
    reqtarpit,
    rspadd,
    rspdel,
    rspdeny,
    rsprep,
    statsAdmin,
    statsAuth,
    statsEnable,
    statsHideVersion,
    statsHttpRequest,
    statsRealm,
    statsRefresh,
    statsScope,
    statsShowDesc,
    statsShowLegends,
    statsShowNode,
    statsUri,
    stickTable,
    tcpRequestConnection,
    tcpRequestContent,
    tcpRequestInspectDelay,
    tcpRequestSession,
    timeoutClient,
    timeoutClientFin,
    timeoutClitimeout,
    timeoutHttpKeepAlive,
    timeoutHttpRequest,
    timeoutTarpit,
    uniqueIdFormat,
    uniqueIdHeader,
    useBackend,
];
const listen = [
    acl,
    backlog,
    balance,
    bind,
    bindProcess,
    block,
    captureCookie,
    captureRequestHeader,
    captureResponseHeader,
    clitimeout,
    compression,
    contimeout,
    cookie,
    declareCapture,
    defaultServer,
    defaultBackend,
    description,
    disabled,
    dispatch,
    dynamicCookieKey,
    emailAlertFrom,
    emailAlertLevel,
    emailAlertMailers,
    emailAlertMyhostname,
    emailAlertTo,
    enabled,
    errorfile,
    errorloc,
    errorloc302,
    errorloc303,
    forcePersist,
    filter,
    fullconn,
    grace,
    hashType,
    httpCheckDisableOn404,
    httpCheckExpect,
    httpCheckSend,
    httpCheckSendState,
    httpRequest,
    httpRequestAddAcl,
    httpRequestAddHeader,
    httpRequestAllow,
    httpRequestAuth,
    httpRequestCacheUse,
    httpRequestCapture,
    httpRequestDelAcl,
    httpRequestDelHeader,
    httpRequestDelMap,
    httpRequestDeny,
    httpRequestDisable17Retry,
    httpRequestDoResolve,
    httpRequestEarlyHint,
    httpRequestRedirect,
    httpRequestReject,
    httpRequestReplaceHeader,
    httpRequestReplacePath,
    httpRequestReplaceUri,
    httpRequestReplaceValue,
    httpRequestScIncGpc0,
    httpRequestScIncGpc1,
    httpRequestScSetGpt0,
    httpRequestSetDst,
    httpRequestSetDstPort,
    httpRequestSetHeader,
    httpRequestSetLogLevel,
    httpRequestSetMap,
    httpRequestSetMark,
    httpRequestSetMethod,
    httpRequestSetNice,
    httpRequestSetPath,
    httpRequestSetPriorityClass,
    httpRequestSetPriorityOffset,
    httpRequestSetQuery,
    httpRequestSetSrc,
    httpRequestSetSrcPort,
    httpRequestSetTos,
    httpRequestSetUri,
    httpRequestSetVar,
    httpRequestSendSpoeGroup,
    httpRequestSilentDrop,
    httpRequestTarpit,
    httpRequestTrackSc0,
    httpRequestTrackSc1,
    httpRequestTrackSc2,
    httpRequestUnsetVar,
    httpRequestUseService,
    httpRequestWaitForHandshake,
    httpResponse,
    httpResponseAddAcl,
    httpResponseAddHeader,
    httpResponseAllow,
    httpResponseCacheStore,
    httpResponseCapture,
    httpResponseDelAcl,
    httpResponseDelHeader,
    httpResponseDelMap,
    httpResponseDeny,
    httpResponseRedirect,
    httpResponseReplaceHeader,
    httpResponseReplaceValue,
    httpResponseScIncGpc0,
    httpResponseScIncGpc1,
    httpResponseScSetGpt0,
    httpResponseSendSpoeGroup,
    httpResponseSetHeader,
    httpResponseSetLogLevel,
    httpResponseSetMap,
    httpResponseSetMark,
    httpResponseSetNice,
    httpResponseSetStatus,
    httpResponseSetTos,
    httpResponseSetVar,
    httpResponseSilentDrop,
    httpResponseTrackSc0,
    httpResponseTrackSc1,
    httpResponseTrackSc2,
    httpResponseUnsetVar,
    httpReuse,
    httpSendNameHeader,
    id,
    ignorePersist,
    loadServerStateFromFile,
    log,
    logFormat,
    logFormatSd,
    logTag,
    maxKeepAliveQueue,
    maxconn,
    mode,
    monitorFail,
    monitorNet,
    monitorUri,
    optionAbortonclose,
    optionAcceptInvalidHttpRequest,
    optionAcceptInvalidHttpResponse,
    optionAllbackups,
    optionCheckcache,
    optionClitcpka,
    optionContstats,
    optionDisableH2Upgrade,
    optionDontlogNormal,
    optionDontlognull,
    optionForwardfor,
    optionH1CaseAdjustBogusClient,
    optionH1CaseAdjustBogusServer,
    optionHttpBufferRequest,
    optionHttpIgnoreProbes,
    optionHttpKeepAlive,
    optionHttpNoDelay,
    optionHttpPretendKeepalive,
    optionHttpServerClose,
    optionHttpTunnel,
    optionHttpUseProxyHeader,
    optionHttpUseHtx,
    optionHttpchk,
    optionHttpclose,
    optionHttplog,
    optionHttpProxy,
    optionIndependentStreams,
    optionLdapCheck,
    optionExternalCheck,
    optionLogHealthChecks,
    optionLogSeparateErrors,
    optionLogasap,
    optionMysqlCheck,
    optionNolinger,
    optionOriginalto,
    optionPersist,
    optionPgsqlCheck,
    optionPreferLastServer,
    optionRedispatch,
    optionRedisCheck,
    optionSmtpchk,
    optionSocketStats,
    optionSpliceAuto,
    optionSpliceRequest,
    optionSpliceResponse,
    optionSpopCheck,
    optionSrvtcpka,
    optionSslHelloChk,
    optionTcpCheck,
    optionTcpSmartAccept,
    optionTcpSmartConnect,
    optionTcpka,
    optionTcplog,
    optionTransparent,
    externalCheckCommand,
    externalCheckPath,
    persistRdpCookie,
    rateLimitSessions,
    redirect,
    redisp,
    redispatch,
    reqadd,
    reqallow,
    reqdel,
    reqdeny,
    reqpass,
    reqrep,
    reqtarpit,
    retries,
    retryOn,
    rspadd,
    rspdel,
    rspdeny,
    rsprep,
    server,
    serverStateFileName,
    serverTemplate,
    source,
    srvtimeout,
    statsAdmin,
    statsAuth,
    statsEnable,
    statsHideVersion,
    statsHttpRequest,
    statsRealm,
    statsRefresh,
    statsScope,
    statsShowDesc,
    statsShowLegends,
    statsShowNode,
    statsUri,
    stickMatch,
    stickOn,
    stickStoreRequest,
    stickStoreResponse,
    stickTable,
    tcpCheckConnect,
    tcpCheckExpect,
    tcpCheckSend,
    tcpCheckSendBinary,
    tcpRequestConnection,
    tcpRequestContent,
    tcpRequestInspectDelay,
    tcpRequestSession,
    tcpResponseContent,
    tcpResponseInspectDelay,
    timeoutCheck,
    timeoutClient,
    timeoutClientFin,
    timeoutClitimeout,
    timeoutConnect,
    timeoutContimeout,
    timeoutHttpKeepAlive,
    timeoutHttpRequest,
    timeoutQueue,
    timeoutServer,
    timeoutServerFin,
    timeoutSrvtimeout,
    timeoutTarpit,
    timeoutTunnel,
    transparent,
    uniqueIdFormat,
    uniqueIdHeader,
    useBackend,
    useServer,
];
const backend = [
    acl,
    balance,
    bindProcess,
    block,
    compression,
    contimeout,
    cookie,
    defaultServer,
    description,
    disabled,
    dispatch,
    dynamicCookieKey,
    emailAlertFrom,
    emailAlertLevel,
    emailAlertMailers,
    emailAlertMyhostname,
    emailAlertTo,
    enabled,
    errorfile,
    errorloc,
    errorloc302,
    errorloc303,
    forcePersist,
    filter,
    fullconn,
    grace,
    hashBalanceFactor,
    hashType,
    httpCheckDisableOn404,
    httpCheckExpect,
    httpCheckSend,
    httpCheckSendState,
    httpRequest,
    httpRequestAddAcl,
    httpRequestAddHeader,
    httpRequestAllow,
    httpRequestAuth,
    httpRequestCacheUse,
    httpRequestCapture,
    httpRequestDelAcl,
    httpRequestDelHeader,
    httpRequestDelMap,
    httpRequestDeny,
    httpRequestDisable17Retry,
    httpRequestDoResolve,
    httpRequestEarlyHint,
    httpRequestRedirect,
    httpRequestReject,
    httpRequestReplaceHeader,
    httpRequestReplacePath,
    httpRequestReplaceUri,
    httpRequestReplaceValue,
    httpRequestScIncGpc0,
    httpRequestScIncGpc1,
    httpRequestScSetGpt0,
    httpRequestSetDst,
    httpRequestSetDstPort,
    httpRequestSetHeader,
    httpRequestSetLogLevel,
    httpRequestSetMap,
    httpRequestSetMark,
    httpRequestSetMethod,
    httpRequestSetNice,
    httpRequestSetPath,
    httpRequestSetPriorityClass,
    httpRequestSetPriorityOffset,
    httpRequestSetQuery,
    httpRequestSetSrc,
    httpRequestSetSrcPort,
    httpRequestSetTos,
    httpRequestSetUri,
    httpRequestSetVar,
    httpRequestSendSpoeGroup,
    httpRequestSilentDrop,
    httpRequestTarpit,
    httpRequestTrackSc0,
    httpRequestTrackSc1,
    httpRequestTrackSc2,
    httpRequestUnsetVar,
    httpRequestUseService,
    httpRequestWaitForHandshake,
    httpResponse,
    httpResponseAddAcl,
    httpResponseAddHeader,
    httpResponseAllow,
    httpResponseCacheStore,
    httpResponseCapture,
    httpResponseDelAcl,
    httpResponseDelHeader,
    httpResponseDelMap,
    httpResponseDeny,
    httpResponseRedirect,
    httpResponseReplaceHeader,
    httpResponseReplaceValue,
    httpResponseScIncGpc0,
    httpResponseScIncGpc1,
    httpResponseScSetGpt0,
    httpResponseSendSpoeGroup,
    httpResponseSetHeader,
    httpResponseSetLogLevel,
    httpResponseSetMap,
    httpResponseSetMark,
    httpResponseSetNice,
    httpResponseSetStatus,
    httpResponseSetTos,
    httpResponseSetVar,
    httpResponseSilentDrop,
    httpResponseTrackSc0,
    httpResponseTrackSc1,
    httpResponseTrackSc2,
    httpResponseUnsetVar,
    httpReuse,
    httpSendNameHeader,
    id,
    ignorePersist,
    loadServerStateFromFile,
    log,
    logTag,
    maxKeepAliveQueue,
    mode,
    optionAbortonclose,
    optionAcceptInvalidHttpResponse,
    optionAllbackups,
    optionCheckcache,
    optionForwardfor,
    optionH1CaseAdjustBogusServer,
    optionHttpBufferRequest,
    optionHttpKeepAlive,
    optionHttpNoDelay,
    optionHttpPretendKeepalive,
    optionHttpServerClose,
    optionHttpUseHtx,
    optionHttpchk,
    optionHttpclose,
    optionHttpProxy,
    optionIndependentStreams,
    optionLdapCheck,
    optionExternalCheck,
    optionLogHealthChecks,
    optionMysqlCheck,
    optionNolinger,
    optionOriginalto,
    optionPersist,
    optionPgsqlCheck,
    optionPreferLastServer,
    optionRedispatch,
    optionRedisCheck,
    optionSmtpchk,
    optionSpliceAuto,
    optionSpliceRequest,
    optionSpliceResponse,
    optionSpopCheck,
    optionSrvtcpka,
    optionSslHelloChk,
    optionTcpCheck,
    optionTcpSmartConnect,
    optionTcpka,
    optionTcplog,
    optionTransparent,
    externalCheckCommand,
    externalCheckPath,
    persistRdpCookie,
    redirect,
    redisp,
    redispatch,
    reqadd,
    reqallow,
    reqdel,
    reqdeny,
    reqpass,
    reqrep,
    reqtarpit,
    retries,
    retryOn,
    rspadd,
    rspdel,
    rspdeny,
    rsprep,
    server,
    serverStateFileName,
    serverTemplate,
    source,
    srvtimeout,
    statsAdmin,
    statsAuth,
    statsEnable,
    statsHideVersion,
    statsHttpRequest,
    statsRealm,
    statsRefresh,
    statsScope,
    statsShowDesc,
    statsShowLegends,
    statsShowNode,
    statsUri,
    stickMatch,
    stickOn,
    stickStoreRequest,
    stickStoreResponse,
    stickTable,
    tcpCheckConnect,
    tcpCheckExpect,
    tcpCheckSend,
    tcpCheckSendBinary,
    tcpRequestContent,
    tcpRequestInspectDelay,
    tcpResponseContent,
    tcpResponseInspectDelay,
    timeoutCheck,
    timeoutConnect,
    timeoutContimeout,
    timeoutHttpKeepAlive,
    timeoutHttpRequest,
    timeoutQueue,
    timeoutServer,
    timeoutServerFin,
    timeoutSrvtimeout,
    timeoutTarpit,
    timeoutTunnel,
    transparent,
    useServer,
];

const all = [
    acl,
    backlog,
    balance,
    bind,
    bindProcess,
    block,
    captureCookie,
    captureRequestHeader,
    captureResponseHeader,
    clitimeout,
    compression,
    contimeout,
    cookie,
    declareCapture,
    defaultServer,
    defaultBackend,
    description,
    disabled,
    dispatch,
    dynamicCookieKey,
    emailAlertFrom,
    emailAlertLevel,
    emailAlertMailers,
    emailAlertMyhostname,
    emailAlertTo,
    enabled,
    errorfile,
    errorloc,
    errorloc302,
    errorloc303,
    forcePersist,
    filter,
    fullconn,
    grace,
    hashType,
    httpCheckDisableOn404,
    httpCheckExpect,
    httpCheckSendState,
    httpRequest,
    httpRequestAddAcl,
    httpRequestAddHeader,
    httpRequestAllow,
    httpRequestAuth,
    httpRequestCacheUse,
    httpRequestCapture,
    httpRequestDelAcl,
    httpRequestDelHeader,
    httpRequestDelMap,
    httpRequestDeny,
    httpRequestDisable17Retry,
    httpRequestDoResolve,
    httpRequestEarlyHint,
    httpRequestRedirect,
    httpRequestReject,
    httpRequestReplaceHeader,
    httpRequestReplacePath,
    httpRequestReplaceUri,
    httpRequestReplaceValue,
    httpRequestScIncGpc0,
    httpRequestScIncGpc1,
    httpRequestScSetGpt0,
    httpRequestSetDst,
    httpRequestSetDstPort,
    httpRequestSetHeader,
    httpRequestSetLogLevel,
    httpRequestSetMap,
    httpRequestSetMark,
    httpRequestSetMethod,
    httpRequestSetNice,
    httpRequestSetPath,
    httpRequestSetPriorityClass,
    httpRequestSetPriorityOffset,
    httpRequestSetQuery,
    httpRequestSetSrc,
    httpRequestSetSrcPort,
    httpRequestSetTos,
    httpRequestSetUri,
    httpRequestSetVar,
    httpRequestSendSpoeGroup,
    httpRequestSilentDrop,
    httpRequestTarpit,
    httpRequestTrackSc0,
    httpRequestTrackSc1,
    httpRequestTrackSc2,
    httpRequestUnsetVar,
    httpRequestUseService,
    httpRequestWaitForHandshake,
    httpResponse,
    httpResponseAddAcl,
    httpResponseAddHeader,
    httpResponseAllow,
    httpResponseCacheStore,
    httpResponseCapture,
    httpResponseDelAcl,
    httpResponseDelHeader,
    httpResponseDelMap,
    httpResponseDeny,
    httpResponseRedirect,
    httpResponseReplaceHeader,
    httpResponseReplaceValue,
    httpResponseScIncGpc0,
    httpResponseScIncGpc1,
    httpResponseScSetGpt0,
    httpResponseSendSpoeGroup,
    httpResponseSetHeader,
    httpResponseSetLogLevel,
    httpResponseSetMap,
    httpResponseSetMark,
    httpResponseSetNice,
    httpResponseSetStatus,
    httpResponseSetTos,
    httpResponseSetVar,
    httpResponseSilentDrop,
    httpResponseTrackSc0,
    httpResponseTrackSc1,
    httpResponseTrackSc2,
    httpResponseUnsetVar,
    httpReuse,
    httpSendNameHeader,
    id,
    ignorePersist,
    loadServerStateFromFile,
    log,
    logFormat,
    logFormatSd,
    logTag,
    maxKeepAliveQueue,
    maxconn,
    mode,
    monitorFail,
    monitorNet,
    monitorUri,
    optionAbortonclose,
    optionAcceptInvalidHttpRequest,
    optionAcceptInvalidHttpResponse,
    optionAllbackups,
    optionCheckcache,
    optionClitcpka,
    optionContstats,
    optionDisableH2Upgrade,
    optionDontlogNormal,
    optionDontlognull,
    optionForwardfor,
    optionH1CaseAdjustBogusClient,
    optionH1CaseAdjustBogusServer,
    optionHttpBufferRequest,
    optionHttpIgnoreProbes,
    optionHttpKeepAlive,
    optionHttpNoDelay,
    optionHttpPretendKeepalive,
    optionHttpServerClose,
    optionHttpTunnel,
    optionHttpUseProxyHeader,
    optionHttpUseHtx,
    optionHttpchk,
    optionHttpclose,
    optionHttplog,
    optionHttpProxy,
    optionIndependentStreams,
    optionLdapCheck,
    optionExternalCheck,
    optionLogHealthChecks,
    optionLogSeparateErrors,
    optionLogasap,
    optionMysqlCheck,
    optionNolinger,
    optionOriginalto,
    optionPersist,
    optionPgsqlCheck,
    optionPreferLastServer,
    optionRedispatch,
    optionRedisCheck,
    optionSmtpchk,
    optionSocketStats,
    optionSpliceAuto,
    optionSpliceRequest,
    optionSpliceResponse,
    optionSpopCheck,
    optionSrvtcpka,
    optionSslHelloChk,
    optionTcpCheck,
    optionTcpSmartAccept,
    optionTcpSmartConnect,
    optionTcpka,
    optionTcplog,
    optionTransparent,
    externalCheckCommand,
    externalCheckPath,
    persistRdpCookie,
    rateLimitSessions,
    redirect,
    redisp,
    redispatch,
    reqadd,
    reqallow,
    reqdel,
    reqdeny,
    reqpass,
    reqrep,
    reqtarpit,
    retries,
    retryOn,
    rspadd,
    rspdel,
    rspdeny,
    rsprep,
    server,
    serverStateFileName,
    serverTemplate,
    source,
    srvtimeout,
    statsAdmin,
    statsAuth,
    statsEnable,
    statsHideVersion,
    statsHttpRequest,
    statsRealm,
    statsRefresh,
    statsScope,
    statsShowDesc,
    statsShowLegends,
    statsShowNode,
    statsUri,
    stickMatch,
    stickOn,
    stickStoreRequest,
    stickStoreResponse,
    stickTable,
    tcpCheckConnect,
    tcpCheckExpect,
    tcpCheckSend,
    tcpCheckSendBinary,
    tcpRequestConnection,
    tcpRequestContent,
    tcpRequestInspectDelay,
    tcpRequestSession,
    tcpResponseContent,
    tcpResponseInspectDelay,
    timeoutCheck,
    timeoutClient,
    timeoutClientFin,
    timeoutClitimeout,
    timeoutConnect,
    timeoutContimeout,
    timeoutHttpKeepAlive,
    timeoutHttpRequest,
    timeoutQueue,
    timeoutServer,
    timeoutServerFin,
    timeoutSrvtimeout,
    timeoutTarpit,
    timeoutTunnel,
    transparent,
    uniqueIdFormat,
    uniqueIdHeader,
    useBackend,
    useServer,
];


const make = (e: Entry<any, any, any>[]) => {
    const generators = makeGenerator(...e);
    const combinedValidator = z.object(e.map(entryToObject).reduce((a, b) => ({...a, ...b}), {}));
    const entryValidator = combinedValidator.or(z.array(combinedValidator));

    const generator = (p: any) => {
        if (Array.isArray(p)) return p.map((e) => generators(e)).join('\n');
        return generators(p);
    }

    return {generator, validator: entryValidator};
}

const defaultsSet = make(defaults);
const frontendsSet = make(frontend);
const listensSet = make(listen);
const backendsSet = make(backend);

const sections = z.object({
    default: defaultsSet.validator,
    frontends: z.record(frontendsSet.validator),
    listens: z.record(listensSet.validator),
    backends: z.record(backendsSet.validator),
}).partial().strict();

const generator = (p: any) => {
    let out = '';
    if ('default' in p) out += `\ndefaults\n${indent(defaultsSet.generator(p.default))}`;
    if ('frontends' in p) out += '\n' + Object.entries(p.frontends).map(([k, v]) => `frontend ${k}\n${indent(frontendsSet.generator(v))}`).join('\n');
    if ('backends' in p) out += '\n' + Object.entries(p.backends).map(([k, v]) => `backend ${k}\n${indent(backendsSet.generator(v))}`).join('\n');
    if ('listens' in p) out += '\n' + Object.entries(p.listens).map(([k, v]) => `listen ${k}\n${indent(listensSet.generator(v))}`).join('\n');
    return out;
}

export default {
    validator: sections,
    generator,
};
