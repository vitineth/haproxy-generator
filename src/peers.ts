import {z} from "zod";
import {indent, makeEntry, optional, passthrough, sizeZod, stringToString, stringZod} from "./utils";

const fallback = <T>(a: T | undefined) => a ?? '';

const name = makeEntry(
    'name',
    stringZod.describe(`<peersect>: Creates a new peer list with name <peersect>. It is an independent section, which is referenced by one or more stick-tables.`),
    passthrough('peers'),
);

const bind = makeEntry(
    'bind',
    z.object({
        address: stringZod.optional(),
        port: z.number().max(65565),
        params: z.array(stringZod).optional(),
    }).optional().describe(`[<address>]:<port_range> [, ...] [param*]: Defines the binding parameters of the local peer of this "peers" section. Such lines are not supported with "peer" line in the same "peers" section.`),
    (v) => v ? `bind ${v.address ? stringToString(v.address) : ''}:${v.port} ${v.params ? v.params.map(stringToString).join(' ') : ''}` : '',
);

const disabled = makeEntry(
    'disabled',
    z.boolean().optional().describe(`Disables a peers section. It disables both listening and any synchronization related to this section. This is provided to disable synchronization of stick tables without having to comment out all "peers" references.`),
    (v) => optional('disabled')(v ?? false),
);

const defaultBind = makeEntry(
    'default-bind',
    z.array(stringZod).optional().describe(`[param*]: Defines the binding parameters for the local peer, excepted its address.`),
    (v) => v ? passthrough('default-bind')(v.map(stringToString).join(' ')) : '',
);

const defaultServer = makeEntry(
    'default-server',
    z.array(stringZod).optional().describe(`[param*]: Change default options for a server in a "peers" section.`),
    (v) => v ? passthrough('default-server')(v.map(stringToString).join(' ')) : '',
);

const enabled = makeEntry(
    'enabled',
    z.boolean().optional().describe(`This re-enables a peers section which was previously disabled via the "disabled" keyword.`),
    (v) => optional('enabled')(v ?? false),
);

const peer = makeEntry(
    'peer',
    z.array(z.object({
        peername: stringZod,
        ip: stringZod,
        port: z.number().max(65565),
        params: z.array(stringZod).optional(),
    })).optional().describe(`<peername> <ip>:<port> [param*]: Defines a peer inside a peers section. If <peername> is set to the local peer name (by default hostname, or forced using "-L" command line option), haproxy will listen for incoming remote peer connection on <ip>:<port>. Otherwise, <ip>:<port> defines where to connect to to join the remote peer, and <peername> is used at the protocol level to identify and validate the remote peer on the server side. During a soft restart, local peer <ip>:<port> is used by the old instance to connect the new one and initiate a complete replication (teaching process). It is strongly recommended to have the exact same peers declaration on all peers and to only rely on the "-L" command line argument to change the local peer name. This makes it easier to maintain coherent configuration files across all peers. You may want to reference some environment variables in the address parameter, see section 2.3 about environment variables. Note: "peer" keyword may transparently be replaced by "server" keyword (see "server" keyword explanation below).`),
    (x) => x ? x.map((v) => `peer ${stringToString(v.peername)} ${stringToString(v.ip)}:${v.port} ${v.params ? v.params.map(stringToString).join(' ') : ''}`).join('\n') : '',
);

const server = makeEntry(
    'server',
    z.array(z.object({
        peername: stringZod,
        address: z.object({
            ip: stringZod,
            port: z.number().max(65565),
        }).optional(),
        params: z.array(stringZod).optional(),
    })).optional().describe(`<peername> <ip>:<port> [param*]: As previously mentioned, "peer" keyword may be replaced by "server" keyword with a support for all "server" parameters found in 5.2 paragraph that are related to transport settings. If the underlying peer is local, <ip>:<port> parameters must not be present; these parameters must be provided on a "bind" line (see "bind" keyword of this "peers" section). A number of "server" parameters are irrelevant for "peers" sections. Peers by nature do not support dynamic host name resolution nor health checks, hence parameters like "init_addr", "resolvers", "check", "agent-check", or "track" are not supported. Similarly, there is no load balancing nor stickiness, thus parameters such as "weight" or "cookie" have no effect.`),
    (x) => x ? x.map((v) => `server ${stringToString(v.peername)} ${v.address ? stringToString(v.address.ip) + ':' + v.address.port : ''} ${v.params ? v.params.map(stringToString).join(' ') : ''}`).join('\n') : '',
);

const table = makeEntry(
    'table',
    z.object({
        tablename: stringZod,
        type: z.enum(['ip', 'integer', 'string']).or(z.object({
            type: z.enum(['string', 'binary']),
            length: z.number().optional(),
        })),
        size: sizeZod,
        expire: stringZod.optional(),
        nopurge: z.boolean().optional(),
        store: z.array(stringZod).optional(),
    }).optional().describe(`<tablename> type {ip | integer | string [len <length>] | binary [len <length>]} size <size> [expire <expire>] [nopurge] [store <data_type>]*: Configure a stickiness table for the current section. This line is parsed exactly the same way as the "stick-table" keyword in others section, except for the "peers" argument which is not required here and with an additional mandatory first parameter to designate the stick-table. Contrary to others sections, there may be several "table" lines in "peers" sections (see also "stick-table" keyword).
            Also be aware of the fact that "peers" sections have their own stick-table namespaces to avoid collisions between stick-table names identical in different "peers" section. This is internally handled prepending the "peers" sections names to the name of the stick-tables followed by a '/' character. If somewhere else in the configuration file you have to refer to such stick-tables declared in "peers" sections you must use the prefixed version of the stick-table name as follows:
            
                peers mypeers
                    peer A ...
                    peer B ...
                    table t1 ...
            
                frontend fe1
                    tcp-request content track-sc0 src table mypeers/t1
            
            This is also this prefixed version of the stick-table names which must be used to refer to stick-tables through the CLI.
            About "peers" protocol, as only "peers" belonging to the same section may communicate with each others, there is no need to do such a distinction. Several "peers" sections may declare stick-tables with the same name. This is shorter version of the stick-table name  which is sent over the network. There is only a '/' character as prefix to avoid stick-table name collisions between stick-tables declared as backends and stick-table declared in "peers" sections as follows in this weird but supported configuration:
            
                peers mypeers
                    peer A ...
                    peer B ...
                    table t1 type string size 10m store gpc0
            
                backend t1
                    stick-table type string size 10m store gpc0 peers mypeers
            
            Here "t1" table declared in "mypeeers" section has "mypeers/t1" as global name. "t1" table declared as a backend as "t1" as global name. But at peer protocol level the former table is named "/t1", the latter is again named "t1".`),
    (v) => {
        if(!v) return '';
        let out = `table ${stringToString(v.tablename)} type `;
        if (typeof (v.type) === 'object') {
            out += `${v.type.type}${v.type.length ? ` len ${v.type.length}` : ''}`;
        } else {
            out += v.type;
        }
        out += ` size ${v.size}`;
        if (v.expire) out += ` expire ${stringToString(v.expire)}`;
        if (v.nopurge) out += ` nopurge`;
        if (v.store) out += ' ' + v.store.map((e) => `store ${stringToString(e)}`).join(' ');
        return out;
    },
)

const peersEntry = z.array(z.object({
    name: name.validator,
    bind: bind.validator,
    disabled: disabled.validator,
    "default-bind": defaultBind.validator,
    "default-server": defaultServer.validator,
    enabled: enabled.validator,
    peer: peer.validator,
    server: server.validator,
    table: table.validator,
}));

const peers = makeEntry(
    'peers',
    peersEntry,
    (v) => v.map((e) => {
        let out = name.generator(e.name);
        if (e.bind) out += `\n${indent(bind.generator(e.bind))}`;
        if (e.disabled) out += `\n${indent(disabled.generator(e.disabled))}`;
        if (e["default-bind"]) out += `\n${indent(defaultBind.generator(e["default-bind"]))}`;
        if (e["default-server"]) out += `\n${indent(defaultServer.generator(e["default-server"]))}`;
        if (e.enabled) out += `\n${indent(enabled.generator(e.enabled))}`;
        if (e.peer) out += `\n${indent(peer.generator(e.peer))}`;
        if (e.server) out += `\n${indent(server.generator(e.server))}`;
        if (e.table) out += `\n${indent(table.generator(e.table))}`;
        return out;
    }).join('\n'),
)

export default {
    generator: peers.generator,
    validator: peers.validator,
}
