import {indent, makeEntry, passthrough, stringToString, stringZod} from "./utils";
import {z} from "zod";

const name = makeEntry(
    'name',
    stringZod.describe(`This is a new program section, this section will create an instance <name> which is visible in "show proc" on the master CLI. (See "9.4. Master CLI" in the management guide).`),
    passthrough('program'),
);

const command = makeEntry(
    'command',
    z.object({
        command: stringZod,
        arguments: z.array(stringZod).optional(),
    }).describe(`Define the command to start with optional arguments. The command is looked up in the current PATH if it does not include an absolute path. This is a mandatory option of the program section. Arguments containing spaces must be enclosed in quotes or double quotes or be prefixed by a backslash.`),
    (v) => `command ${stringToString(v.command)} ${v.arguments ? v.arguments.map(stringToString).join(' ') : ''}`,
);

const optionStartOnReload = makeEntry(
    'start-on-reload',
    z.boolean().optional().describe(`Start (or not) a new instance of the program upon a reload of the master. The default is to start a new instance. This option may only be used in a program section.`),
    (v) => v ? 'option start-on-reload' : 'no option start-on-reload',
);

const programEntry = z.array(z.object({
    name: name.validator,
    command: command.validator,
    "start-on-reload": optionStartOnReload.validator,
}));

const programs = makeEntry(
    'programs',
    programEntry,
    (v) => v.map((e) => {
        let out = name.generator(e.name);
        out += `\n${indent(command.generator(e.command))}`;
        if (e["start-on-reload"] !== undefined) out += `\n${indent(optionStartOnReload.generator(e["start-on-reload"]))}`;
        return out;
    }).join('\n'),
);

export default {
    generator: programs.generator,
    validator: programs.validator,
};
