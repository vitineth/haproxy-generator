import {string, z} from "zod";
import * as util from "util";
import {stringZod, timeZod} from "./utils";
import globals from "./globals";
import userlists from "./userlists";
import peers from "./peers";
import mailers from "./mailers";
import programs from "./programs";
import proxy from "./proxy";

const config = z.object({
    global: globals.validator,
    userlists: userlists.validator,
    peers: peers.validator,
    mailers: mailers.validator,
    programs: programs.validator,
    proxies: proxy.validator,
}).partial().strict();


const flat = (a: { success: true, data: any } | { success: false, error: any }) => a.success ? a.data : a.error.issues;

const validate = config.safeParse(
    require('../haproxy.config.json')
);
if (validate.success) {
    // console.log(
    //     util.inspect(
    //         validate.data,
    //         false,
    //         null,
    //         true
    //     )
    // );
    //
    // console.log('\n============\n');

    if ('global' in validate.data && Object.hasOwn(validate.data, 'global')) {
        console.log(globals.generator(validate.data.global!))
    }
    if ('userlists' in validate.data && Object.hasOwn(validate.data, 'userlists')) {
        console.log(userlists.generator(validate.data.userlists!))
    }
    if ('peers' in validate.data && Object.hasOwn(validate.data, 'peers')) {
        console.log(peers.generator(validate.data.peers!))
    }
    if ('mailers' in validate.data && Object.hasOwn(validate.data, 'mailers')) {
        console.log(mailers.generator(validate.data.mailers!))
    }
    if ('programs' in validate.data && Object.hasOwn(validate.data, 'programs')) {
        console.log(programs.generator(validate.data.programs!))
    }
    if ('proxies' in validate.data && Object.hasOwn(validate.data, 'proxies')) {
        console.log(proxy.generator(validate.data.proxies!))
    }
}else{
    console.log(util.inspect(validate.error, false, null, true));
}

