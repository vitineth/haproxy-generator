import {z} from "zod";
import {
    cpuMapRegex,
    entryToObject,
    makeEntry,
    makeGenerator, optional,
    passthrough, stringArrayPassthrough,
    stringToString,
    stringZod,
    timeRegex
} from "./utils";


const caBase = makeEntry(
    "ca-base",
    stringZod.describe(`<dir>: Assigns a default directory to fetch SSL CA certificates and CRLs from when a relative path is used with "ca-file" or "crl-file" directives. Absolute locations specified in "ca-file" and "crl-file" prevail and ignore "ca-base".`),
    passthrough("ca-base"),
);

const chroot = makeEntry(
    "chroot",
    stringZod.describe(`<jail dir>: Changes current directory to <jail dir> and performs a chroot() there before dropping privileges. This increases the security level in case an unknown vulnerability would be exploited, since it would make it very hard for the attacker to exploit the system. This only works when the process is started with superuser privileges. It is important to ensure that <jail_dir> is both empty and non-writable to anyone.`),
    passthrough("chroot"),
);
const crtBase = makeEntry(
    "crt-base",
    stringZod.describe(`<dir>: Assigns a default directory to fetch SSL certificates from when a relative path is used with "crtfile" directives. Absolute locations specified after "crtfile" prevail and ignore "crt-base".`),
    passthrough("crt-base"),
);
const cpuMap = makeEntry(
    "cpu-map",
    z.string().regex(cpuMapRegex).describe(`[auto:]<process-set>[/<thread-set>] <cpu-set>: On Linux 2.6 and above, it is possible to bind a process or a thread to a specific CPU set. This means that the process or the thread will never run on other CPUs. The "cpu-map" directive specifies CPU sets for process or thread sets. The first argument is a process set, eventually followed by a thread set. These sets have the format
        all | odd | even | number[-[number]]
        <number>> must be a number between 1 and 32 or 64, depending on the machine's word size. Any process IDs above nbproc and any thread IDs above nbthread are ignored. It is possible to specify a range with two such number delimited by a dash ('-'). It also is possible to specify all processes at once using "all", only odd numbers using "odd" or even numbers using "even", just like with the "bind-process" directive. The second and forthcoming arguments are CPU sets. Each CPU set is either a unique number between 0 and 31 or 63 or a range with two such numbers delimited by a dash ('-'). Multiple CPU numbers or ranges may be specified, and the processes or threads will be allowed to bind to all of them. Obviously, multiple "cpu-map" directives may be specified. Each "cpu-map" directive will replace the previous ones when they overlap. A thread will be bound on the intersection of its mapping and the one of the process on which it is attached. If the intersection is null, no specific binding will be set for the thread.
        Ranges can be partially defined. The higher bound can be omitted. In such case, it is replaced by the corresponding maximum value, 32 or 64 depending on the machine's word size.
        The prefix "auto:" can be added before the process set to let HAProxy automatically bind a process or a thread to a CPU by incrementing process/thread and CPU sets. To be valid, both sets must have the same size. No matter the declaration order of the CPU sets, it will be bound from the lowest to the highest bound. Having a process and a thread range with the "auto:" prefix is not supported. Only one range is supported, the other one must be a fixed number.`),
    passthrough("cpu-map"),
)
const daemon = makeEntry(
    "daemon",
    z.literal(true).describe(`Makes the process fork into background. This is the recommended mode of operation. It is equivalent to the command line "-D" argument. It can be disabled by the command line "-db" argument. This option is ignored in systemd mode.`),
    optional('daemon'),
);
const description = makeEntry(
    "description",
    stringZod.describe(`<text>: Add a text that describes the instance. Please note that it is required to escape certain characters (# for example) and this text is inserted into a html page so you should avoid using "<" and ">" characters.`),
    passthrough("description"),
);
const deviceatlasJsonFile = makeEntry(
    "deviceatlas-json-file",
    stringZod.describe(`<path>: Sets the path of the DeviceAtlas JSON data file to be loaded by the API. The path must be a valid JSON data file and accessible by HAProxy process`),
    passthrough("deviceatlas-json-file"),
);
const deviceatlasLogLevel = makeEntry(
    "deviceatlas-log-level",
    stringZod.describe(`<value>: Sets the level of information returned by the API. This directive is optional and set to 0 by default if not set.`),
    passthrough("deviceatlas-log-level"),
);
const deviceatlasSeparator = makeEntry(
    "deviceatlas-separator",
    z.string().length(1).describe(`<char>: Sets the character separator for the API properties results. This directive is optional and set to | by default if not set.`),
    passthrough("deviceatlas-separator"),
);
const deviceatlasPropertiesCookie = makeEntry(
    "deviceatlas-properties-cookie",
    stringZod.describe(`<name>: Sets the client cookie's name used for the detection if the DeviceAtlas Client-side component was used during the request. This directive is optional and set to DAPROPS by default if not set.`),
    passthrough("deviceatlas-properties-cookie"),
);
const externalCheck = makeEntry(
    "external-check",
    z.literal(true).describe(`Allows the use of an external agent to perform health checks. This is disabled by default as a security precaution. See "option external-check".`),
    optional('external-check'),
);
const gid = makeEntry(
    "gid",
    z.number().describe(`<gid>: Changes the process's group ID to <number>. It is recommended that the group ID is dedicated to HAProxy or to a small set of similar daemons. HAProxy must be started with a user belonging to this group, or with superuser privileges. Note that if haproxy is started from a user having supplementary groups, it will only be able to drop these groups if started with superuser privileges. See also "group" and "uid".`),
    passthrough("gid"),
);
const group = makeEntry(
    "group",
    stringZod.describe(`<group name> : Similar to "gid" but uses the GID of group name <group name> from /etc/group. See also "gid" and "user".`),
    passthrough("group"),
);
const hardStopAfter = makeEntry(
    "hard-stop-after",
    z.string().regex(timeRegex).describe(`<time>: Defines the maximum time allowed to perform a clean soft-stop. This is the the maximum time (by default in milliseconds) for which the instance will remain alive when a soft-stop is received via the SIGUSR1 signal. This may be used to ensure that the instance will quit even if connections remain opened during a soft-stop (for example with long timeouts for a proxy in tcp mode). It applies both in TCP and HTTP mode`),
    passthrough("hard-stop-after"),
);
const h1CaseAdjust = makeEntry(
    "h1-case-adjust", z.array(stringZod).describe(`<from> <to> (note: specify the to here, the from will be automatically created from the lower case version: Defines the case adjustment to apply, when enabled, to the header name <from>, to change it to <to> before sending it to HTTP/1 clients or servers. <from> must be in lower case, and <from> and <to> must not differ except for their case. It may be repeated if several header names need to be adjusted. Duplicate entries are not allowed. If a lot of header names have to be adjusted, it might be more convenient to use "h1-case-adjust-file". Please note that no transformation will be applied unless "option h1-case-adjust-bogus-client" or "option h1-case-adjust-bogus-server" is specified in a proxy.
        There is no standard case for header names because, as stated in RFC7230, they are case-insensitive. So applications must handle them in a case- insensitive manner. But some bogus applications violate the standards and erroneously rely on the cases most commonly used by browsers. This problem becomes critical with HTTP/2 because all header names must be exchanged in lower case, and HAProxy follows the same convention. All header names are sent in lower case to clients and servers, regardless of the HTTP version.
        Applications which fail to properly process requests or responses may require to temporarily use such workarounds to adjust header names sent to them for the time it takes the application to be fixed. Please note that an application which requires such workarounds might be vulnerable to content smuggling attacks and must absolutely be fixed`),
    stringArrayPassthrough('h1-case-adjust'),
);
const h1CaseAdjustFile = makeEntry(
    "h1-case-adjust-file", z.array(stringZod).describe(`<hdrs-file>: Defines a file containing a list of key/value pairs used to adjust the case of some header names before sending them to HTTP/1 clients or servers. The file <hdrs-file> must contain 2 header names per line. The first one must be in lower case and both must not differ except for their case. Lines which start with '#' are ignored, just like empty lines. Leading and trailing tabs and spaces are stripped. Duplicate entries are not allowed. Please note that no transformation will be applied unless "option h1-case-adjust-bogus-client" or "option h1-case-adjust-bogus-server" is specified in a proxy.
        If this directive is repeated, only the last one will be processed.  It is an alternative to the directive "h1-case-adjust" if a lot of header names need to be adjusted. Please read the risks associated with using this.`),
    stringArrayPassthrough('h1-case-adjust-file'),
);
const log = makeEntry(
    "log", z.array(z.object({
        address: stringZod,
        facility: z.array(z.enum(['kern', 'user', 'mail', 'daemon', 'auth', 'syslog', 'lpr', 'news', 'uucp', 'cron', 'auth2', 'ftp', 'ntp', 'audit', 'alert', 'cron2', 'local0', 'local1', 'local2', 'local3', 'local4', 'local5', 'local6', 'local7'])),
        len: z.number().optional(),
        format: z.enum(['rfc3164', 'rfc5424', 'short', 'raw']).optional(),
        sample: stringZod.optional(),
        max: z.enum(['emerg', 'alert', 'crit', 'err', 'warning', 'notice', 'info', 'debug']).optional(),
        min: z.enum(['emerg', 'alert', 'crit', 'err', 'warning', 'notice', 'info', 'debug']).optional(),
    })).describe(`<address> [len <length>] [format <format>] [sample <ranges>:<smp_size>] <facility> [max level [min level]]: Adds a global syslog server. Several global servers can be defined. They will receive logs for starts and exits, as well as all logs from proxies configured with "log global".
                <address> can be one of:
                      - An IPv4 address optionally followed by a colon and a UDP port. If no port is specified, 514 is used by default (the standard syslog port).
                      - An IPv6 address followed by a colon and optionally a UDP port. If no port is specified, 514 is used by default (the standard syslog port).
                      - A filesystem path to a datagram UNIX domain socket, keeping in mind considerations for chroot (be sure the path is accessible inside the chroot) and uid/gid (be sure the path is appropriately writable).
                      - A file descriptor number in the form "fd@<number>", which may point to a pipe, terminal, or socket. In this case unbuffered logs are used and one writev() call per log is performed. This is a bit expensive but acceptable for most workloads. Messages sent this way will not be truncated but may be dropped, in which case the DroppedLogs counter will be incremented. The writev() call is atomic even on pipes for messages up to PIPE_BUF size, which POSIX recommends to be at least 512 and which is 4096 bytes on most modern operating systems. Any larger message may be interleaved with messages from other processes. Exceptionally for debugging purposes the file descriptor may also be directed to a file, but doing so will significantly slow haproxy down as non-blocking calls will be ignored. Also there will be no way to purge nor rotate this file without restarting the process. Note that the configured syslog format is preserved, so the output is suitable for use with a TCP syslog server. See also the "short" and "raw" format below.
                      - "stdout" / "stderr", which are respectively aliases for "fd@1" and "fd@2", see above.
                   You may want to reference some environment variables in the address parameter, see section 2.3 about environment variables.
                <length> is an optional maximum line length. Log lines larger than this value will be truncated before being sent. The reason is that syslog servers act differently on log line length. All servers support the default value of 1024, but some servers simply drop larger lines while others do log them. If a server supports long lines, it may make sense to set this value here in order to avoid truncating long lines. Similarly, if a server drops long lines, it is preferable to truncate them before sending them. Accepted values are 80 to 65535 inclusive. The default value of 1024 is generally fine for all standard usages. Some specific cases of long captures or JSON-formatted logs may require larger values. You may also need to increase "tune.http.logurilen" if your request URIs are truncated.
                <format> is the log format used when generating syslog messages. It may be one of the following :
                  rfc3164   The RFC3164 syslog message format. This is the default. (https://tools.ietf.org/html/rfc3164)
                  rfc5424   The RFC5424 syslog message format. (https://tools.ietf.org/html/rfc5424)
                  short     A message containing only a level between angle brackets such as '<3>', followed by the text. The PID, date, time, process name and system name are omitted. This is designed to be used with a local log server. This format is compatible with what the systemd logger consumes.
                  raw       A message containing only the text. The level, PID, date, time, process name and system name are omitted. This is designed to be used in containers or during development, where the severity only depends on the file descriptor used (stdout/stderr).
                <ranges>   A list of comma-separated ranges to identify the logs to sample. This is used to balance the load of the logs to send to the log server. The limits of the ranges cannot be null. They are numbered from 1. The size or period (in number of logs) of the sample must be set with <sample_size> parameter.
                <sample_size> The size of the sample in number of logs to consider when balancing their logging loads. It is used to balance the load of the logs to send to the syslog server. This size must be greater or equal to the maximum of the high limits of the ranges. (see also <ranges> parameter).
                <facility> must be one of the 24 standard syslog facilities :
                               kern   user   mail   daemon auth   syslog lpr    news
                               uucp   cron   auth2  ftp    ntp    audit  alert  cron2
                               local0 local1 local2 local3 local4 local5 local6 local7
                           Note that the facility is ignored for the "short" and "raw" formats, but still required as a positional field. It is recommended to use "daemon" in this case to make it clear that it's only supposed to be used locally.
                
                An optional level can be specified to filter outgoing messages. By default, all messages are sent. If a maximum level is specified, only messages with a severity at least as important as this level will be sent. An optional minimum level can be specified. If it is set, logs emitted with a more severe level than this one will be capped to this level. This is used to avoid sending "emerg" messages on all terminals on some default syslog configurations. Eight levels are known :
                        emerg  alert  crit   err    warning notice info  debug`),
    (v) =>
        v.map((e) => {
            let out = `log ${e.address}`;
            if (e.len) out += ` len ${e.len}`;
            if (e.format) out += ` format ${e.format}`;
            if (e.sample) out += ` sample ${stringToString(e.sample)}`;
            out += ` ${e.facility.join(' ')}`;
            if (e.max) out += ` ${e.max}`;
            if (e.min) out += ` ${e.min}`;
            return out;
        }).join('\n'),
);

const logTag = makeEntry(
    "log-tag",
    stringZod.describe(`<string>: Sets the tag field in the syslog header to this string. It defaults to the program name as launched from the command line, which usually is "haproxy". Sometimes it can be useful to differentiate between multiple processes running on the same host. See also the per-proxy "log-tag" directive.`),
    passthrough('log-tag'),
);
const logSendHostname = makeEntry(
    "log-send-hostname",
    z.literal(true).or(stringZod).describe(`[<string>]: Sets the hostname field in the syslog header. If optional "string" parameter is set the header is set to the string contents, otherwise uses the hostname of the system. Generally used if one is not relaying logs through an intermediate syslog server or for simply customizing the hostname printed in the logs.`),
    (v) => typeof (v) === 'boolean' ? optional('log-send-hostname')(v) : passthrough('log-send-hostname')(v),
);
const luaLoad = makeEntry(
    "lua-load",
    z.array(stringZod).describe(`<file>: This global directive loads and executes a Lua file. This directive can be used multiple times.`),
    stringArrayPassthrough('lua-load'),
);
const masterWorker = makeEntry(
    "master-worker",
    z.literal(true).or(z.literal('no-exit-on-failure')).describe(`[no-exit-on-failure]: Master-worker mode. It is equivalent to the command line "-W" argument. This mode will launch a "master" which will monitor the "workers". Using this mode, you can reload HAProxy directly by sending a SIGUSR2 signal to the master. The master-worker mode is compatible either with the foreground or daemon mode. It is recommended to use this mode with multiprocess and systemd. By default, if a worker exits with a bad return code, in the case of a segfault for example, all workers will be killed, and the master will leave. It is convenient to combine this behavior with Restart=on-failure in a systemd unit file in order to relaunch the whole process. If you don't want this behavior, you must use the keyword "no-exit-on-failure". See also "-W" in the management guide.`),
    (v) => typeof (v) === 'boolean' ? optional('master-worker')(v) : passthrough('master-worker')(v),
);
const mworkerMaxReloads = makeEntry(
    "mworker-max-reloads",
    z.number().describe(`<number> In master-worker mode, this option limits the number of time a worker can survive to a reload. If the worker did not leave after a reload, once its number of reloads is greater than this number, the worker will receive a SIGTERM. This option helps to keep under control the number of workers. See also "show proc" in the Management Guide. `),
    passthrough('mworker-max-reloads'),
);
const nbproc = makeEntry(
    "nbproc",
    z.number().describe(`<number>: Creates <number> processes when going daemon. This requires the "daemon" mode. By default, only one process is created, which is the recommended mode of operation. For systems limited to small sets of file descriptors per process, it may be needed to fork multiple daemons. When set to a value larger than 1, threads are automatically disabled. USING MULTIPLE PROCESSES IS HARDER TO DEBUG AND IS REALLY DISCOURAGED. See also "daemon" and "nbthread".`),
    passthrough('nbproc'),
);
const nbthread = makeEntry(
    "nbthread",
    z.number().describe(`<number>: This setting is only available when support for threads was built in. It makes haproxy run on <number> threads. This is exclusive with "nbproc". While "nbproc" historically used to be the only way to use multiple processors, it also involved a number of shortcomings related to the lack of synchronization between processes (health-checks, peers, stick-tables, stats, ...) which do not affect threads. As such, any modern configuration is strongly encouraged to migrate away from "nbproc" to "nbthread". "nbthread" also works when HAProxy is started in foreground. On some platforms supporting CPU affinity, when nbproc is not used, the default "nbthread" value is automatically set to the number of CPUs the process is bound to upon startup. This means that the thread count can easily be adjusted from the calling process using commands like "taskset" or "cpuset". Otherwise, this value defaults to 1. The default value is reported in the output of "haproxy -vv". See also "nbproc".`),
    passthrough('nbthread'),
);
const node = makeEntry(
    "node",
    stringZod.describe(`<name>: Only letters, digits, hyphen and underscore are allowed, like in DNS names. This statement is useful in HA configurations where two or more processes or servers share the same IP address. By setting a different node-name on all nodes, it becomes easy to immediately spot what server is handling the traffic.`),
    passthrough('node'),
);

const pidFile = makeEntry(
    "pidfile",
    stringZod.describe(`<pidfile>: Writes PIDs of all daemons into file <pidfile>. This option is equivalent to the "-p" command line argument. The file must be accessible to the user starting the process. See also "daemon".`),
    passthrough('pidfile'),
);

const presetenv = makeEntry(
    "presetenv", z.array(z.object({
        name: stringZod,
        value: stringZod,
    })).describe(`<name> <value>: Sets environment variable <name> to value <value>. If the variable exists, it is NOT overwritten. The changes immediately take effect so that the next line in the configuration file sees the new value. See also "setenv", "resetenv", and "unsetenv".`),
    (v) => v.map((e) => `presetenv ${stringToString(e.name)} ${stringToString(e.value)}`).join('\n'),
);

const setenv = makeEntry(
    "setenv", z.array(z.object({
        name: stringZod,
        value: stringZod,
    })).describe(`<name> <value>: Sets environment variable <name> to value <value>. If the variable exists, it is overwritten. The changes immediately take effect so that the next line in the configuration file sees the new value. See also "presetenv", "resetenv", and "unsetenv".`),
    (v) => v.map((e) => `setenv ${stringToString(e.name)} ${stringToString(e.value)}`).join('\n'),
);

const stats = makeEntry(
    "stats", z.object({
        "bind-process": z.array(z.string().regex(/(all|odd|even|[0-9]+(-([0-9]+)?)?)/)).optional().describe(`[ all | odd | even | <process_num>[-[process_num>]] ] ...: Limits the stats socket to a certain set of processes numbers. By default the stats socket is bound to all processes, causing a warning to be emitted when nbproc is greater than 1 because there is no way to select the target process when connecting. However, by using this setting, it becomes possible to pin the stats socket to a specific set of processes, typically the first one. The warning will automatically be disabled when this setting is used, whatever the number of processes used. The maximum process ID depends on the machine's word size (32 or 64). Ranges can be partially defined. The higher bound can be omitted. In such case, it is replaced by the corresponding maximum value. A better option consists in using the "process" setting of the "stats socket" line to force the process on each line.`),
        "socket": z.object({
            "address": stringZod,
            "params": z.array(stringZod).optional(),
        }).optional().describe(`[<address:port>|<path>] [param*]: Binds a UNIX socket to <path> or a TCPv4/v6 address to <address:port>. Connections to this socket will return various statistics outputs and even allow some commands to be issued to change some runtime settings. Please consult section 9.3 "Unix Socket commands" of Management Guide for more details. All parameters supported by "bind" lines are supported, for instance to restrict access to some users or their access rights. Please consult section 5.1 for more information.`),
        "timeout": z.string().regex(timeRegex).optional().describe(`<timeout, in milliseconds>: The default timeout on the stats socket is set to 10 seconds. It is possible to change this value with "stats timeout". The value must be passed in milliseconds, or be suffixed by a time unit among { us, ms, s, m, h, d }.`),
        "maxconn": z.number().optional().describe(`<connections>: By default, the stats socket is limited to 10 concurrent connections. It is possible to change this value with "stats maxconn".`),
    }),
    (v) => {
        let out = ``;
        if (v["bind-process"]) {
            out += '\n' + v["bind-process"].map((e) => `stats bind-process ${e}`).join('\n');
        }
        if (v.socket) {
            out += `\nstats socket ${stringToString(v.socket.address)} ${v.socket.params ? v.socket.params.map(stringToString).join(' ') : ''}`;
        }
        if (v.timeout) {
            out += `\nstats timeout ${v.timeout}`;
        }
        if (v.maxconn) {
            out += `\nstats maxconn ${v.maxconn}`;
        }

        return out;
    },
);

const unixBind = makeEntry(
    "unix-bind", z.object({
        "prefix": stringZod,
        "mode": stringZod,
        "user": stringZod,
        "uid": z.number(),
        "group": stringZod,
        "gid": z.number(),
    }).partial().describe(`[ prefix <prefix> ] [ mode <mode> ] [ user <user> ] [ uid <uid> ] [ group <group> ] [ gid <gid> ]: Fixes common settings to UNIX listening sockets declared in "bind" statements. This is mainly used to simplify declaration of those UNIX sockets and reduce the risk of errors, since those settings are most commonly required but are also process-specific. The <prefix> setting can be used to force all socket path to be relative to that directory. This might be needed to access another component's chroot. Note that those paths are resolved before haproxy chroots itself, so they are absolute. The <mode>, <user>, <uid>, <group> and <gid> all have the same meaning as their homonyms used by the "bind" statement. If both are specified, the "bind" statement has priority, meaning that the "unix-bind" settings may be seen as process-wide default settings.`),
    (v) => `unix-bind ${v.prefix ? `prefix ${v.prefix} ` : ' '}${v.mode ? `mode ${v.mode} ` : ' '}${v.user ? `user ${v.user} ` : ' '}${v.uid ? `uid ${v.uid} ` : ' '}${v.group ? `group ${v.group} ` : ' '}${v.gid ? `gid ${v.gid} ` : ' '}`,
);

const sslEngine = makeEntry(
    "ssl-engine",
    stringZod.or(z.object({
        "name": stringZod,
        "algo": z.array(stringZod),
    })).describe(`<name> [algo <comma-separated list of algorithms>]: Sets the OpenSSL engine to <name>. List of valid values for <name> may be obtained using the command "openssl engine". This statement may be used multiple times, it will simply enable multiple crypto engines. Referencing an unsupported engine will prevent haproxy from starting. Note that many engines will lead to lower HTTPS performance than pure software with recent processors. The optional command "algo" sets the default algorithms an ENGINE will supply using the OPENSSL function ENGINE_set_default_string(). A value of "ALL" uses the engine for all cryptographic operations. If no list of algo is specified then the value of "ALL" is used. A comma-separated list of different algorithms may be specified, including: RSA, DSA, DH, EC, RAND, CIPHERS, DIGESTS, PKEY, PKEY_CRYPTO, PKEY_ASN1. This is the same format that openssl configuration file uses: https://www.openssl.org/docs/man1.0.2/apps/config.html`),
    (v) => {
        if (typeof (v) === 'string') return passthrough('ssl-engine')(v);
        if ('name' in v) {
            return passthrough('ssl-engine')(`${v.name} ${v.algo.map(stringToString).join(',')}`)
        } else {
            return passthrough('ssl-engine')(v);
        }
    },
);

const resetenv = makeEntry(
    "resetenv",
    z.literal(true).or(z.array(stringZod)).describe(`[<name>...]: Sets environment variable <name> to value <value>. If the variable exists, it is NOT overwritten. The changes immediately take effect so that the next line in the configuration file sees the new value. See also "setenv", "resetenv", and "unsetenv".`),
    (v) => typeof (v) === 'boolean' ? optional('resetenv')(v) : passthrough('resetenv')(v.map((e) => stringToString(e)).join(' ')),
);
const uid = makeEntry(
    "uid",
    z.number().describe(`<number>: Changes the process's user ID to <number>. It is recommended that the user ID is dedicated to HAProxy or to a small set of similar daemons. HAProxy must be started with superuser privileges in order to be able to switch to another one. See also "gid" and "user".`),
    passthrough('uid'),
);
const ulimitN = makeEntry(
    "ulimit-n",
    z.number().describe(`<number>: Sets the maximum number of per-process file-descriptors to <number>. By default, it is automatically computed, so it is recommended not to use this option.`),
    passthrough('ulimit-n'),
);
const user = makeEntry(
    "user",
    stringZod.describe(`<user name>: Similar to "uid" but uses the UID of user name <user name> from /etc/passwd. See also "uid" and "group".`),
    passthrough('user'),
);
const setDumpable = makeEntry(
    "set-dumpable",
    z.boolean().describe(`This option is better left disabled by default and enabled only upon a developer's request. It has no impact on performance nor stability but will try hard to re-enable core dumps that were possibly disabled by file size limitations (ulimit -f), core size limitations (ulimit -c), or "dumpability" of a process after changing its UID/GID (such as /proc/sys/fs/suid_dumpable on Linux). Core dumps might still be limited by the current directory's permissions (check what directory the file is started from), the chroot directory's permission (it may be needed to temporarily disable the chroot directive or to move it to a dedicated writable location), or any other system-specific constraint. For example, some Linux flavours are notorious for replacing the default core file with a path to an executable not even installed on the system (check /proc/sys/kernel/core_pattern). Often, simply writing "core", "core.%p" or "/var/log/core/core.%p" addresses the issue. When trying to enable this option waiting for a rare issue to re-appear, it's often a good idea to first try to obtain such a dump by issuing, for example, "kill -11" to the haproxy process and verify that it leaves a core where expected when dying.`),
    optional('set-dumpable'),
);
const sslDefaultBindCiphers = makeEntry(
    "ssl-default-bind-ciphers",
    stringZod.describe(`<ciphers>: This setting is only available when support for OpenSSL was built in. It sets the default string describing the list of cipher algorithms ("cipher suite") that are negotiated during the SSL/TLS handshake up to TLSv1.2 for all "bind" lines which do not explicitly define theirs. The format of the string is defined in "man 1 ciphers" from OpenSSL man pages. For background information and recommendations see e.g. (https://wiki.mozilla.org/Security/Server_Side_TLS) and (https://mozilla.github.io/server-side-tls/ssl-config-generator/). For TLSv1.3 cipher configuration, please check the "ssl-default-bind-ciphersuites" keyword. Please check the "bind" keyword for more information.`),
    passthrough('ssl-default-bind-ciphers'),
);
const sslDefaultBindCiphersuites = makeEntry(
    "ssl-default-bind-ciphersuites",
    stringZod.describe(`<ciphersuites>: This setting is only available when support for OpenSSL was built in and OpenSSL 1.1.1 or later was used to build HAProxy. It sets the default string describing the list of cipher algorithms ("cipher suite") that are negotiated during the TLSv1.3 handshake for all "bind" lines which do not explicitly define theirs. The format of the string is defined in "man 1 ciphers" from OpenSSL man pages under the section "ciphersuites". For cipher configuration for TLSv1.2 and earlier, please check the "ssl-default-bind-ciphers" keyword. Please check the "bind" keyword for more information.`),
    passthrough('ssl-default-bind-ciphersuites'),
);
const sslDefaultBindOptions = makeEntry(
    "ssl-default-bind-options",
    z.array(stringZod).describe(`[<option>]...: This setting is only available when support for OpenSSL was built in. It sets default ssl-options to force on all "bind" lines. Please check the "bind" keyword to see available options.`),
    (v) => passthrough('ssl-default-bind-options')(v.map((e) => stringToString(e)).join(' ')),
);
const sslDefaultServerCiphers = makeEntry(
    "ssl-default-server-ciphers",
    stringZod.describe(`<ciphers>: This setting is only available when support for OpenSSL was built in. It sets the default string describing the list of cipher algorithms that are negotiated during the SSL/TLS handshake up to TLSv1.2 with the server, for all "server" lines which do not explicitly define theirs. The format of the string is defined in "man 1 ciphers" from OpenSSL man pages. For background information and recommendations see e.g. (https://wiki.mozilla.org/Security/Server_Side_TLS) and (https://mozilla.github.io/server-side-tls/ssl-config-generator/). For TLSv1.3 cipher configuration, please check the "ssl-default-server-ciphersuites" keyword. Please check the "server" keyword for more information.`),
    passthrough('ssl-default-server-ciphers'),
);
const sslDefaultServerCiphersuites = makeEntry(
    "ssl-default-server-ciphersuites",
    stringZod.describe(`<ciphersuites>: This setting is only available when support for OpenSSL was built in and OpenSSL 1.1.1 or later was used to build HAProxy. It sets the default string describing the list of cipher algorithms that are negotiated during the TLSv1.3 handshake with the server, for all "server" lines which do not explicitly define theirs. The format of the string is defined in "man 1 ciphers" from OpenSSL man pages under the section "ciphersuites". For cipher configuration for TLSv1.2 and earlier, please check the "ssl-default-server-ciphers" keyword. Please check the "server" keyword for more information.`),
    passthrough('ssl-default-server-cipersuites'),
);
const sslDefaultServerOptions = makeEntry(
    "ssl-default-server-options",
    z.literal(true).or(z.array(stringZod)).describe(`[<option>]...: This setting is only available when support for OpenSSL was built in. It sets default ssl-options to force on all "server" lines. Please check the "server" keyword to see available options.`),
    (v) => typeof (v) === 'boolean' ? optional('ssl-default-server-options')(v) : passthrough('ssl-default-server-options')(v.map(stringToString).join(' ')),
);
const sslDhParamFile = makeEntry(
    "ssl-dh-param-file",
    stringZod.describe(`<file>: This setting is only available when support for OpenSSL was built in. It sets the default DH parameters that are used during the SSL/TLS handshake when ephemeral Diffie-Hellman (DHE) key exchange is used, for all "bind" lines which do not explicitly define theirs. It will be overridden by custom DH parameters found in a bind certificate file if any. If custom DH parameters are not specified either by using ssl-dh-param-file or by setting them directly in the certificate file, pre-generated DH parameters of the size specified by tune.ssl.default-dh-param will be used. Custom parameters are known to be more secure and therefore their use is recommended. Custom DH parameters may be generated by using the OpenSSL command "openssl dhparam <size>", where size should be at least 2048, as 1024-bit DH parameters should not be considered secure anymore.`),
    passthrough('ssl-dh-param-file'),
);
const sslServerVerify = makeEntry(
    "ssl-server-verify",
    z.literal(true).or(z.enum(['none', 'required'])).describe(`[none|required]: The default behavior for SSL verify on servers side. If specified to 'none', servers certificates are not verified. The default is 'required' except if forced using cmdline option '-dV'.`),
    (v) => typeof (v) === 'boolean' ? optional('ssl-server-verify')(v) : passthrough('ssl-server-verify')(v),
);
const unsetenv = makeEntry(
    "unsetenv",
    z.array(stringZod).describe(`[<name> ...]: Removes environment variables specified in arguments. This can be useful to hide some sensitive information that are occasionally inherited from the user's environment during some operations. Variables which did not exist are silently ignored so that after the operation, it is certain that none of these variables remain. The changes immediately take effect so that the next line in the configuration file will not see these variables. See also "setenv", "presetenv", and "resetenv".`),
    (v) => passthrough('unsetenv')(v.map(stringToString).join(' ')),
);
const fiftyOneDegreesDataFile = makeEntry(
    "51degrees-data-file",
    stringZod.describe(`<file path>: The path of the 51Degrees data file to provide device detection services. The file should be unzipped and accessible by HAProxy with relevant permissions. Please note that this option is only available when haproxy has been compiled with USE_51DEGREES.`),
    passthrough('51degrees-data-file'),
);
const fiftyOneDegreesPropertyNameList = makeEntry(
    "51degrees-property-name-list",
    z.array(stringZod).describe(`[<string>...]: A list of 51Degrees property names to be load from the dataset. A full list of names is available on the 51Degrees website: https://51degrees.com/resources/property-dictionary Please note that this option is only available when haproxy has been compiled with USE_51DEGREES.`),
    (v) => passthrough('51degrees-property-name-list')(v.map(stringToString).join(' ')),
);
const fiftyOneDegreesPropertySeparator = makeEntry(
    "51degrees-property-separator",
    z.string().length(1).describe(`<char>: A char that will be appended to every property value in a response header containing 51Degrees results. If not set that will be set as ','. Please note that this option is only available when haproxy has been compiled with USE_51DEGREES.`),
    passthrough('51degrees-property-separator'),
);
const fiftyOneDegreesCacheSize = makeEntry(
    "51degrees-cache-size",
    z.number().describe(`<number>: Sets the size of the 51Degrees converter cache to <number> entries. This is an LRU cache which reminds previous device detections and their results. By default, this cache is disabled. Please note that this option is only available when haproxy has been compiled with USE_51DEGREES.`),
    passthrough('51degrees-cache-size'),
);
const wurflDataFile = makeEntry(
    "wurfl-data-file",
    stringZod.describe(`<file path>: The path of the WURFL data file to provide device detection services. The file should be accessible by HAProxy with relevant permissions. Please note that this option is only available when haproxy has been compiled with USE_WURFL=1.`),
    passthrough('wurfl-data-file'),
);
const wurflInformationList = makeEntry(
    "wurfl-information-list",
    z.array(stringZod).describe(`[<capability>]*: A space-delimited list of WURFL capabilities, virtual capabilities, property names we plan to use in injected headers. A full list of capability and virtual capability names is available on the Scientiamobile website : https://www.scientiamobile.com/wurflCapability  Valid WURFL properties are: (wurfl_id: Contains the device ID of the matched device) (wurfl_root_id: Contains the device root ID of the matched device) (wurfl_isdevroot: Tells if the matched device is a root device. Possible values are "TRUE" or "FALSE") (wurfl_useragent: The original useragent coming with this particular web request) (wurfl_api_version: Contains a string representing the currently used Libwurfl API version) (wurfl_info: A string containing information on the parsed wurfl.xml and its full path) (wurfl_last_load_time: Contains the UNIX timestamp of the last time WURFL has been loaded successfully) (wurfl_normalized_useragent: The normalized useragent) Please note that this option is only available when haproxy has been compiled with USE_WURFL=1.`),
    (v) => passthrough('wurfl-information-list')(v.map(stringToString).join(' ')),
);
const wurflInformationListSeparator = makeEntry(
    "wurfl-information-list-separator",
    z.string().length(1).describe(`<char>: A char that will be used to separate values in a response header containing WURFL results. If not set that a comma (',') will be used by default. Please note that this option is only available when haproxy has been compiled with USE_WURFL=1.`),
    passthrough('wurfl-information-list-separator'),
);
const wurflPathFile = makeEntry(
    "wurfl-path-file",
    z.array(stringZod).describe(`[<file path>]: A list of WURFL patch file paths. Note that patches are loaded during startup thus before the chroot. Please note that this option is only available when haproxy has been compiled with USE_WURFL=1.`),
    stringArrayPassthrough('wurfl-path-file'), // TODO: can't tell if this should be repeated or on the same line
);
const wurflCacheSize = makeEntry(
    "wurfl-cache-size",
    z.number().describe(`<size>: Sets the WURFL Useragent cache size. For faster lookups, already processed user agents are kept in a LRU cache : - "0"     : no cache is used. - <size>  : size of lru cache in elements. Please note that this option is only available when haproxy has been compiled with USE_WURFL=1.`),
    passthrough('wurfl-cache-size'),
);
const maxSpreadChecks = makeEntry(
    "max-spread-checks",
    z.string().regex(timeRegex).describe(`<delay in milliseconds>: By default, haproxy tries to spread the start of health checks across the smallest health check interval of all the servers in a farm. The principle is to avoid hammering services running on the same server. But when using large check intervals (10 seconds or more), the last servers in the farm take some time before starting to be tested, which can be a problem. This parameter is used to enforce an upper bound on delay between the first and the last check, even if the servers' check intervals are larger. When servers run with shorter intervals, their intervals will be respected though.`),
    passthrough('max-spread-checks'),
);
const maxconn = makeEntry(
    "maxconn",
    z.number().describe(`<number>: Sets the maximum per-process number of concurrent connections to <number>. It is equivalent to the command-line argument "-n". Proxies will stop accepting connections when this limit is reached. The "ulimit-n" parameter is automatically adjusted according to this value. See also "ulimit-n". Note: the "select" poller cannot reliably use more than 1024 file descriptors on some platforms. If your platform only supports select and reports "select FAILED" on startup, you need to reduce maxconn until it works (slightly below 500 in general). If this value is not set, it will automatically be calculated based on the current file descriptors limit reported by the "ulimit -n" command, possibly reduced to a lower value if a memory limit is enforced, based on the buffer size, memory allocated to compression, SSL cache size, and use or not of SSL and the associated maxsslconn (which can also be automatic).`),
    passthrough('maxconn'),
);
const maxconnrate = makeEntry(
    "maxconnrate",
    z.number().describe(`<number>: Sets the maximum per-process number of connections per second to <number>. Proxies will stop accepting connections when this limit is reached. It can be used to limit the global capacity regardless of each frontend capacity. It is important to note that this can only be used as a service protection measure, as there will not necessarily be a fair share between frontends when the limit is reached, so it's a good idea to also limit each frontend to some value close to its expected share. Also, lowering tune.maxaccept can improve fairness.`),
    passthrough('maxconnrate'),
);
const maxcomprate = makeEntry(
    "maxcomprate",
    z.number().describe(`<number>: Sets the maximum per-process input compression rate to <number> kilobytes per second. For each session, if the maximum is reached, the compression level will be decreased during the session. If the maximum is reached at the beginning of a session, the session will not compress at all. If the maximum is not reached, the compression level will be increased up to tune.comp.maxlevel. A value of zero means there is no limit, this is the default value.`),
    passthrough('maxcomprate'),
);
const maxcompcpuusage = makeEntry(
    "maxcompcpuusage",
    z.number().describe(`<number>: Sets the maximum CPU usage HAProxy can reach before stopping the compression for new requests or decreasing the compression level of current requests. It works like 'maxcomprate' but measures CPU usage instead of incoming data bandwidth. The value is expressed in percent of the CPU used by haproxy. In case of multiple processes (nbproc > 1), each process manages its individual usage. A value of 100 disable the limit. The default value is 100. Setting a lower value will prevent the compression work from slowing the whole process down and from introducing high latencies.`),
    passthrough('maxcompcpuusage'),
);
const maxpipes = makeEntry(
    "maxpipes",
    z.number().describe(`<number>: Sets the maximum per-process number of pipes to <number>. Currently, pipes are only used by kernel-based tcp splicing. Since a pipe contains two file descriptors, the "ulimit-n" value will be increased accordingly. The default value is maxconn/4, which seems to be more than enough for most heavy usages. The splice code dynamically allocates and releases pipes, and can fall back to standard copy, so setting this value too low may only impact performance.`),
    passthrough('maxpipes'),
);
const maxsessrate = makeEntry(
    "maxsessrate",
    z.number().describe(`<number>: Sets the maximum per-process number of sessions per second to <number>. Proxies will stop accepting connections when this limit is reached. It can be used to limit the global capacity regardless of each frontend capacity. It is important to note that this can only be used as a service protection measure, as there will not necessarily be a fair share between frontends when the limit is reached, so it's a good idea to also limit each frontend to some value close to its expected share. Also, lowering tune.maxaccept can improve fairness.`),
    passthrough('maxsessrate'),
);
const maxsslconn = makeEntry(
    "maxsslconn",
    z.number().describe(`<number>: Sets the maximum per-process number of concurrent SSL connections to <number>. By default there is no SSL-specific limit, which means that the global maxconn setting will apply to all connections. Setting this limit avoids having openssl use too much memory and crash when malloc returns NULL (since it unfortunately does not reliably check for such conditions). Note that the limit applies both to incoming and outgoing connections, so one connection which is deciphered then ciphered accounts for 2 SSL connections. If this value is not set, but a memory limit is enforced, this value will be automatically computed based on the memory limit, maxconn,  the buffer size, memory allocated to compression, SSL cache size, and use of SSL in either frontends, backends or both. If neither maxconn nor maxsslconn are specified when there is a memory limit, haproxy will automatically adjust these values so that 100% of the connections can be made over SSL with no risk, and will consider the sides where it is enabled (frontend, backend, both).`),
    passthrough('maxsslconn'),
);
const maxsslrate = makeEntry(
    "maxsslrate",
    z.number().describe(`<number>: Sets the maximum per-process number of SSL sessions per second to <number>. SSL listeners will stop accepting connections when this limit is reached. It can be used to limit the global SSL CPU usage regardless of each frontend capacity. It is important to note that this can only be used as a service protection measure, as there will not necessarily be a fair share between frontends when the limit is reached, so it's a good idea to also limit each frontend to some value close to its expected share. It is also important to note that the sessions are accounted before they enter the SSL stack and not after, which also protects the stack against bad handshakes. Also, lowering tune.maxaccept can improve fairness.`),
    passthrough('maxsslrate'),
);
const maxzlibmem = makeEntry(
    "maxzlibmem",
    z.number().describe(`<number>: Sets the maximum amount of RAM in megabytes per process usable by the zlib. When the maximum amount is reached, future sessions will not compress as long as RAM is unavailable. When sets to 0, there is no limit. The default value is 0. The value is available in bytes on the UNIX socket with "show info" on the line "MaxZlibMemUsage", the memory used by zlib is "ZlibMemUsage" in bytes.`),
    passthrough('maxzlibmem'),
);
const noepoll = makeEntry(
    "noepoll",
    z.boolean().describe(`Disables the use of the "epoll" event polling system on Linux. It is equivalent to the command-line argument "-de". The next polling system used will generally be "poll". See also "nopoll".`),
    optional('noepoll'),
);
const nokqueue = makeEntry(
    "nokqueue",
    z.boolean().describe(`Disables the use of the "kqueue" event polling system on BSD. It is equivalent to the command-line argument "-dk". The next polling system used will generally be "poll". See also "nopoll".`),
    optional('nokqueue'),
);
const noevports = makeEntry(
    "noevports",
    z.boolean().describe(`Disables the use of the event ports event polling system on SunOS systems derived from Solaris 10 and later. It is equivalent to the command-line argument "-dv". The next polling system used will generally be "poll". See also "nopoll".`),
    optional('noevports'),
);
const nopoll = makeEntry(
    "nopoll",
    z.boolean().describe(`Disables the use of the "poll" event polling system. It is equivalent to the command-line argument "-dp". The next polling system used will be "select". It should never be needed to disable "poll" since it's available on all platforms supported by HAProxy. See also "nokqueue", "noepoll" and "noevports".`),
    optional('nopoll'),
);
const nosplice = makeEntry(
    "nosplice",
    z.boolean().describe(`Disables the use of kernel tcp splicing between sockets on Linux. It is equivalent to the command line argument "-dS". Data will then be copied using conventional and more portable recv/send calls. Kernel tcp splicing is limited to some very recent instances of kernel 2.6. Most versions between 2.6.25 and 2.6.28 are buggy and will forward corrupted data, so they must not be used. This option makes it easier to globally disable kernel splicing in case of doubt. See also "option splice-auto", "option splice-request" and "option splice-response".`),
    optional('nosplice'),
);
const nogetaddrinfo = makeEntry(
    "nogetaddrinfo",
    z.boolean().describe(`Disables the use of getaddrinfo(3) for name resolving. It is equivalent to the command line argument "-dG". Deprecated gethostbyname(3) will be used.`),
    optional('nogetaddrinfo'),
);
const noreuseport = makeEntry(
    "noreuseport",
    z.boolean().describe(`Disables the use of SO_REUSEPORT - see socket(7). It is equivalent to the command line argument "-dR".`),
    optional('noreuseport'),
);
const profilingTasks = makeEntry(
    "profiling.tasks",
    z.enum(['auto', 'on', 'off']).describe(`{ auto | on | off }: Enables ('on') or disables ('off') per-task CPU profiling. When set to 'auto' the profiling automatically turns on a thread when it starts to suffer from an average latency of 1000 microseconds or higher as reported in the "avg_loop_us" activity field, and automatically turns off when the latency returns below 990 microseconds (this value is an average over the last 1024 loops so it does not vary quickly and tends to significantly smooth short spikes). It may also spontaneously trigger from time to time on overloaded systems, containers, or virtual machines, or when the system swaps (which must absolutely never happen on a load balancer). CPU profiling per task can be very convenient to report where the time is spent and which requests have what effect on which other request. Enabling it will typically affect the overall's performance by less than 1%, thus it is recommended to leave it to the default 'auto' value so that it only operates when a problem is identified. This feature requires a system supporting the clock_gettime(2) syscall with clock identifiers CLOCK_MONOTONIC and CLOCK_THREAD_CPUTIME_ID, otherwise the reported time will be zero. This option may be changed at run time using "set profiling" on the CLI.`),
    passthrough('profiling.tasks'),
);
const spreadChecks = makeEntry(
    "spread-checks",
    z.number().min(0).max(50).describe(`<0..50, in percent>: Sometimes it is desirable to avoid sending agent and health checks to servers at exact intervals, for instance when many logical servers are located on the same physical server. With the help of this parameter, it becomes possible to add some randomness in the check interval between 0 and +/- 50%. A value between 2 and 5 seems to show good results. The default value remains at 0.`),
    passthrough('spread-checks'),
);
const serverStateBase = makeEntry(
    "server-state-base",
    stringZod.describe(`<directory>: Specifies the directory prefix to be prepended in front of all servers state file names which do not start with a '/'. See also "server-state-file", "load-server-state-from-file" and "server-state-file-name".`),
    passthrough('server-state-base'),
);
const serverStateFile = makeEntry(
    "server-state-file",
    stringZod.describe(`<file>: Specifies the path to the file containing state of servers. If the path starts with a slash ('/'), it is considered absolute, otherwise it is considered relative to the directory specified using "server-state-base" (if set) or to the current directory. Before reloading HAProxy, it is possible to save the servers' current state using the stats command "show servers state". The output of this command must be written in the file pointed by <file>. When starting up, before handling traffic, HAProxy will read, load and apply state for each server found in the file and available in its current running configuration. See also "server-state-base" and "show servers state", "load-server-state-from-file" and "server-state-file-name"`),
    passthrough('server-state-file'),
);
const sslModeAsync = makeEntry(
    "ssl-mode-async",
    z.boolean().describe(`Adds SSL_MODE_ASYNC mode to the SSL context. This enables asynchronous TLS I/O operations if asynchronous capable SSL engines are used. The current implementation supports a maximum of 32 engines. The Openssl ASYNC API doesn't support moving read/write buffers and is not compliant with haproxy's buffer management. So the asynchronous mode is disabled on read/write  operations (it is only enabled during initial and renegotiation handshakes).`),
    optional('ssl-mode-async'),
);
const tuneBuffersLimit = makeEntry(
    "tune.buffers.limit",
    z.number().describe(`<number>: Sets a hard limit on the number of buffers which may be allocated per process. The default value is zero which means unlimited. The minimum non-zero value will always be greater than "tune.buffers.reserve" and should ideally always be about twice as large. Forcing this value can be particularly useful to limit the amount of memory a process may take, while retaining a sane behavior. When this limit is reached, sessions which need a buffer wait for another one to be released by another session. Since buffers are dynamically allocated and released, the waiting time is very short and not perceptible provided that limits remain reasonable. In fact sometimes reducing the limit may even increase performance by increasing the CPU cache's efficiency. Tests have shown good results on average HTTP traffic with a limit to 1/10 of the expected global maxconn setting, which also significantly reduces memory usage. The memory savings come from the fact that a number of connections will not allocate 2*tune.bufsize. It is best not to touch this value unless advised to do so by an haproxy core developer.`),
    passthrough('tune.buffers.limit'),
);
const tuneBuffersReserve = makeEntry(
    "tune.buffers.reserve",
    z.number().describe(`<number>: Sets the number of buffers which are pre-allocated and reserved for use only during memory shortage conditions resulting in failed memory allocations. The minimum value is 2 and is also the default. There is no reason a user would want to change this value, it's mostly aimed at haproxy core developers.`),
    passthrough('tune.buffers.reserve'),
);
const tuneBufsize = makeEntry(
    "tune.bufsize",
    z.number().describe(`<number>: Sets the buffer size to this size (in bytes). Lower values allow more sessions to coexist in the same amount of RAM, and higher values allow some applications with very large cookies to work. The default value is 16384 and can be changed at build time. It is strongly recommended not to change this from the default value, as very low values will break some services such as statistics, and values larger than default size will increase memory usage, possibly causing the system to run out of memory. At least the global maxconn parameter should be decreased by the same factor as this one is increased. In addition, use of HTTP/2 mandates that this value must be 16384 or more. If an HTTP request is larger than (tune.bufsize - tune.maxrewrite), haproxy will return HTTP 400 (Bad Request) error. Similarly if an HTTP response is larger than this size, haproxy will return HTTP 502 (Bad Gateway). Note that the value set using this parameter will automatically be rounded up to the next multiple of 8 on 32-bit machines and 16 on 64-bit machines.`),
    passthrough('tune.bufsize'),
);
const tuneChksize = makeEntry(
    "tune.chksize",
    z.number().describe(`<number>: Sets the check buffer size to this size (in bytes). Higher values may help find string or regex patterns in very large pages, though doing so may imply more memory and CPU usage. The default value is 16384 and can be changed at build time. It is not recommended to change this value, but to use better checks whenever possible.`),
    passthrough('tune.chksize'),
);
const tuneCompMaxlevel = makeEntry(
    "tune.comp.maxlevel",
    z.number().describe(`<number>: Sets the maximum compression level. The compression level affects CPU usage during compression. This value affects CPU usage during compression. Each session using compression initializes the compression algorithm with this value. The default value is 1.`),
    passthrough('tune.comp.maxlevel'),
);
const tuneFailAlloc = makeEntry(
    "tune.fail-alloc",
    z.boolean().describe(`If compiled with DEBUG_FAIL_ALLOC, gives the percentage of chances an allocation attempt fails. Must be between 0 (no failure) and 100 (no success). This is useful to debug and make sure memory failures are handled gracefully.`),
    optional('tune.fail-alloc'),
);
const tuneH2HeaderTableSize = makeEntry(
    "tune.h2.header-table-size",
    z.number().describe(`<number>: Sets the HTTP/2 dynamic header table size. It defaults to 4096 bytes and cannot be larger than 65536 bytes. A larger value may help certain clients send more compact requests, depending on their capabilities. This amount of memory is consumed for each HTTP/2 connection. It is recommended not to change it.`),
    passthrough('tune.h2.header-table-size'),
);
const tuneH2InitialWindowSize = makeEntry(
    "tune.h2.initial-window-size",
    z.number().describe(`<number>: Sets the HTTP/2 initial window size, which is the number of bytes the client can upload before waiting for an acknowledgment from haproxy. This setting only affects payload contents (i.e. the body of POST requests), not headers. The default value is 65535, which roughly allows up to 5 Mbps of upload bandwidth per client over a network showing a 100 ms ping time, or 500 Mbps over a 1-ms local network. It can make sense to increase this value to allow faster uploads, or to reduce it to increase fairness when dealing with many clients. It doesn't affect resource usage.`),
    passthrough('tune.h2.initial-window-size'),
);
const tuneH2MaxConcurrentStreams = makeEntry(
    "tune.h2.max-concurrent-streams",
    z.number().describe(`<number>: Sets the HTTP/2 maximum number of concurrent streams per connection (ie the number of outstanding requests on a single connection). The default value is 100. A larger one may slightly improve page load time for complex sites when visited over high latency networks, but increases the amount of resources a single client may allocate. A value of zero disables the limit so a single client may create as many streams as allocatable by haproxy. It is highly recommended not to change this value.`),
    passthrough('tune.h2.max-concurrent-streams'),
);
const tuneH2MaxFrameSize = makeEntry(
    "tune.h2.max-frame-size",
    z.number().describe(`<number>: Sets the HTTP/2 maximum frame size that haproxy announces it is willing to receive to its peers. The default value is the largest between 16384 and the buffer size (tune.bufsize). In any case, haproxy will not announce support for frame sizes larger than buffers. The main purpose of this setting is to allow to limit the maximum frame size setting when using large buffers. Too large frame sizes might have performance impact or cause some peers to misbehave. It is highly recommended not to change this value.`),
    passthrough('tune.h2.max-frame-size'),
);
const tuneHttpCookielen = makeEntry(
    "tune.http.cookielen",
    z.number().describe(`<number>: Sets the maximum length of captured cookies. This is the maximum value that the "capture cookie xxx len yyy" will be allowed to take, and any upper value will automatically be truncated to this one. It is important not to set too high a value because all cookie captures still allocate this size whatever their configured value (they share a same pool). This value is per request per response, so the memory allocated is twice this value per connection. When not specified, the limit is set to 63 characters. It is recommended not to change this value.`),
    passthrough('tune.http.cookielen'),
);
const tuneHttpLogurilen = makeEntry(
    "tune.http.logurilen",
    z.number().describe(`<number>: Sets the maximum length of request URI in logs. This prevents truncating long request URIs with valuable query strings in log lines. This is not related to syslog limits. If you increase this limit, you may also increase the 'log ... len yyy' parameter. Your syslog daemon may also need specific configuration directives too. The default value is 1024.`),
    passthrough('tune.http.logurilen'),
);
const tuneHttpMaxhdr = makeEntry(
    "tune.http.maxhdr",
    z.number().describe(`<number>: Sets the maximum number of headers in a request. When a request comes with a number of headers greater than this value (including the first line), it is rejected with a "400 Bad Request" status code. Similarly, too large responses are blocked with "502 Bad Gateway". The default value is 101, which is enough for all usages, considering that the widely deployed Apache server uses the same limit. It can be useful to push this limit further to temporarily allow a buggy application to work by the time it gets fixed. The accepted range is 1..32767. Keep in mind that each new header consumes 32bits of memory for each session, so don't push this limit too high.`),
    passthrough('tune.http.maxhdr'),
);
const tuneIdletimer = makeEntry(
    "tune.idletimer",
    z.string().regex(timeRegex).or(z.number()).describe(`<timeout>: Sets the duration after which haproxy will consider that an empty buffer is probably associated with an idle stream. This is used to optimally adjust some packet sizes while forwarding large and small data alternatively. The decision to use splice() or to send large buffers in SSL is modulated by this parameter. The value is in milliseconds between 0 and 65535. A value of zero means that haproxy will not try to detect idle streams. The default is 1000, which seems to correctly detect end user pauses (e.g. read a page before clicking). There should be no reason for changing this value. Please check tune.ssl.maxrecord below.`),
    passthrough('tune.idletimer'),
);
const tuneListenerMultiQueue = makeEntry(
    "tune.listener.multi-queue",
    z.enum(['on', 'off']).describe(`{ on | off }: Enables ('on') or disables ('off') the listener's multi-queue accept which spreads the incoming traffic to all threads a "bind" line is allowed to run on instead of taking them for itself. This provides a smoother traffic distribution and scales much better, especially in environments where threads may be unevenly loaded due to external activity (network interrupts colliding with one thread for example). This option is enabled by default, but it may be forcefully disabled for troubleshooting or for situations where it is estimated that the operating system already provides a good enough distribution and connections are extremely short-lived.`),
    passthrough('tune.listener.multi-queue'),
);
const tuneLuaForcedYield = makeEntry(
    "tune.lua.forced-yield",
    z.number().describe(`<number>: This directive forces the Lua engine to execute a yield each <number> of instructions executed. This permits interrupting a long script and allows the HAProxy scheduler to process other tasks like accepting connections or forwarding traffic. The default value is 10000 instructions. If HAProxy often executes some Lua code but more responsiveness is required, this value can be lowered. If the Lua code is quite long and its result is absolutely required to process the data, the <number> can be increased.`),
    passthrough('tune.lua.forced-yield'),
);
const tuneLuaMaxmem = makeEntry(
    "tune.lua.maxmem",
    z.number().describe(`Sets the maximum amount of RAM in megabytes per process usable by Lua. By default it is zero which means unlimited. It is important to set a limit to ensure that a bug in a script will not result in the system running out of memory.`),
    passthrough('tune.lua.maxmem'),
);
const tuneLuaSessionTimeout = makeEntry(
    "tune.lua.session-timeout",
    z.string().regex(timeRegex).or(z.number()).describe(`<timeout>: This is the execution timeout for the Lua sessions. This is useful for preventing infinite loops or spending too much time in Lua. This timeout counts only the pure Lua runtime. If the Lua does a sleep, the sleep is not taken in account. The default timeout is 4s.`),
    passthrough('tune.lua.session-timeout'),
);
const tuneLuaTaskTimeout = makeEntry(
    "tune.lua.task-timeout",
    z.string().regex(timeRegex).or(z.number()).describe(`<timeout>: Purpose is the same as "tune.lua.session-timeout", but this timeout is dedicated to the tasks. By default, this timeout isn't set because a task may remain alive during of the lifetime of HAProxy. For example, a task used to check servers.`),
    passthrough('tune.lua.task-timeout'),
);
const tuneLuaServiceTimeout = makeEntry(
    "tune.lua.service-timeout",
    z.string().regex(timeRegex).or(z.number()).describe(`<timeout>: This is the execution timeout for the Lua services. This is useful for preventing infinite loops or spending too much time in Lua. This timeout counts only the pure Lua runtime. If the Lua does a sleep, the sleep is not taken in account. The default timeout is 4s.`),
    passthrough('tune.lua.service-timeout'),
);
const tuneMaxaccept = makeEntry(
    "tune.maxaccept",
    z.number().describe(`<number>: Sets the maximum number of consecutive connections a process may accept in a row before switching to other work. In single process mode, higher numbers give better performance at high connection rates. However in multi-process modes, keeping a bit of fairness between processes generally is better to increase performance. This value applies individually to each listener, so that the number of processes a listener is bound to is taken into account. This value defaults to 64. In multi-process mode, it is divided by twice the number of processes the listener is bound to. Setting this value to -1 completely disables the limitation. It should normally not be needed to tweak this value.`),
    passthrough('tune.maxaccept'),
);
const tuneMaxpollevents = makeEntry(
    "tune.maxpollevents",
    z.number().describe(`<number>: Sets the maximum amount of events that can be processed at once in a call to the polling system. The default value is adapted to the operating system. It has been noticed that reducing it below 200 tends to slightly decrease latency at the expense of network bandwidth, and increasing it above 200 tends to trade latency for slightly increased bandwidth.`),
    passthrough('tune.maxpollevents'),
);
const tuneMaxrewrite = makeEntry(
    "tune.maxrewrite",
    z.number().describe(`<number>: Sets the reserved buffer space to this size in bytes. The reserved space is used for header rewriting or appending. The first reads on sockets will never fill more than bufsize-maxrewrite. Historically it has defaulted to half of bufsize, though that does not make much sense since there are rarely large numbers of headers to add. Setting it too high prevents processing of large requests or responses. Setting it too low prevents addition of new headers to already large requests or to POST requests. It is generally wise to set it to about 1024. It is automatically readjusted to half of bufsize if it is larger than that. This means you don't have to worry about it when changing bufsize.`),
    passthrough('tune.maxrewrite'),
);
const tunePatternCacheSize = makeEntry(
    "tune.pattern.cache-size",
    z.number().describe(`<number>: Sets the size of the pattern lookup cache to <number> entries. This is an LRU cache which reminds previous lookups and their results. It is used by ACLs and maps on slow pattern lookups, namely the ones using the "sub", "reg", "dir", "dom", "end", "bin" match methods as well as the case-insensitive strings. It applies to pattern expressions which means that it will be able to memorize the result of a lookup among all the patterns specified on a configuration line (including all those loaded from files). It automatically invalidates entries which are updated using HTTP actions or on the CLI. The default cache size is set to 10000 entries, which limits its footprint to about 5 MB per process/thread on 32-bit systems and 8 MB per process/thread on 64-bit systems, as caches are thread/process local. There is a very low risk of collision in this cache, which is in the order of the size of the cache divided by 2^64. Typically, at 10000 requests per second with the default cache size of 10000 entries, there's 1% chance that a brute force attack could cause a single collision after 60 years, or 0.1% after 6 years. This is considered much lower than the risk of a memory corruption caused by aging components. If this is not acceptable, the cache can be disabled by setting this parameter to 0.`),
    passthrough('tune.pattern.cache-size'),
);
const tunePipesize = makeEntry(
    "tune.pipesize",
    z.number().describe(`<number>: Sets the kernel pipe buffer size to this size (in bytes). By default, pipes are the default size for the system. But sometimes when using TCP splicing, it can improve performance to increase pipe sizes, especially if it is suspected that pipes are not filled and that many calls to splice() are performed. This has an impact on the kernel's memory footprint, so this must not be changed if impacts are not understood.`),
    passthrough('tune.pipesize'),
);
const tunePoolHighFdRatio = makeEntry(
    "tune.pool-high-fd-ratio",
    z.number().describe(`<number>: This setting sets the max number of file descriptors (in percentage) used by haproxy globally against the maximum number of file descriptors haproxy can use before we start killing idle connections when we can't reuse a connection and we have to create a new one. The default is 25 (one quarter of the file descriptor will mean that roughly half of the maximum front connections can keep an idle connection behind, anything beyond this probably doesn't make much sense in the general case when targeting connection reuse).`),
    passthrough('tune.pool-high-fd-ratio'),
);
const tunePoolLowFdRatio = makeEntry(
    "tune.pool-low-fd-ratio",
    z.number().describe(`<number>: This setting sets the max number of file descriptors (in percentage) used by haproxy globally against the maximum number of file descriptors haproxy can use before we stop putting connection into the idle pool for reuse. The default is 20.`),
    passthrough('tune.pool-low-fd-ratio'),
);
const tuneRcvbufClient = makeEntry(
    "tune.rcvbuf.client",
    z.number().describe(`<number>`),
    passthrough('tune.rcvbuf.client'),
);
const tuneRcvbufServer = makeEntry(
    "tune.rcvbuf.server",
    z.number().describe(`<number>: Forces the kernel socket receive buffer size on the client or the server side to the specified value in bytes. This value applies to all TCP/HTTP frontends and backends. It should normally never be set, and the default size (0) lets the kernel auto-tune this value depending on the amount of available memory. However it can sometimes help to set it to very low values (e.g. 4096) in order to save kernel memory by preventing it from buffering too large amounts of received data. Lower values will significantly increase CPU usage though.`),
    passthrough('tune.rcvbuf.server'),
);
const tuneRecv_enough = makeEntry(
    "tune.recv_enough",
    z.number().describe(`<number>: HAProxy uses some hints to detect that a short read indicates the end of the socket buffers. One of them is that a read returns more than <recv_enough> bytes, which defaults to 10136 (7 segments of 1448 each). This default value may be changed by this setting to better deal with workloads involving lots of short messages such as telnet or SSH sessions.`),
    passthrough('tune.recv_enough'),
);
const tuneRunqueueDepth = makeEntry(
    "tune.runqueue-depth",
    z.number().describe(`<number>: Sets the maximum amount of task that can be processed at once when running tasks. The default value is 200. Increasing it may incur latency when dealing with I/Os, making it too small can incur extra overhead.`),
    passthrough('tune.runqueue-depth'),
);
const tuneSndbufClient = makeEntry(
    "tune.sndbuf.client",
    z.number().describe(`<number>`),
    passthrough('tune.sndbuf.client'),
);
const tuneSndbufServer = makeEntry(
    "tune.sndbuf.server",
    z.number().describe(`<number>: Forces the kernel socket send buffer size on the client or the server side to the specified value in bytes. This value applies to all TCP/HTTP frontends and backends. It should normally never be set, and the default size (0) lets the kernel auto-tune this value depending on the amount of available memory. However it can sometimes help to set it to very low values (e.g. 4096) in order to save kernel memory by preventing it from buffering too large amounts of received data. Lower values will significantly increase CPU usage though. Another use case is to prevent write timeouts with extremely slow clients due to the kernel waiting for a large part of the buffer to be read before notifying haproxy again.`),
    passthrough('tune.sndbuf.server'),
);
const tuneSslCachesize = makeEntry(
    "tune.ssl.cachesize",
    z.number().describe(`<number>: Sets the size of the global SSL session cache, in a number of blocks. A block is large enough to contain an encoded session without peer certificate. An encoded session with peer certificate is stored in multiple blocks depending on the size of the peer certificate. A block uses approximately 200 bytes of memory. The default value may be forced at build time, otherwise defaults to 20000. When the cache is full, the most idle entries are purged and reassigned. Higher values reduce the occurrence of such a purge, hence the number of CPU-intensive SSL handshakes by ensuring that all users keep their session as long as possible. All entries are pre-allocated upon startup and are shared between all processes if "nbproc" is greater than 1. Setting this value to 0 disables the SSL session cache.`),
    passthrough('tune.ssl.cachesize'),
);
const tuneSslLifetime = makeEntry(
    "tune.ssl.lifetime",
    z.string().regex(timeRegex).or(z.number()).describe(`<timeout>: Sets how long a cached SSL session may remain valid. This time is expressed in seconds and defaults to 300 (5 min). It is important to understand that it does not guarantee that sessions will last that long, because if the cache is full, the longest idle sessions will be purged despite their configured lifetime. The real usefulness of this setting is to prevent sessions from being used for too long.`),
    passthrough('tune.ssl.lifetime'),
);
const tuneSslForcePrivateCache = makeEntry(
    "tune.ssl.force-private-cache",
    z.boolean().describe(`This option disables SSL session cache sharing between all processes. It should normally not be used since it will force many renegotiations due to clients hitting a random process. But it may be required on some operating systems where none of the SSL cache synchronization method may be used. In this case, adding a first layer of hash-based load balancing before the SSL layer might limit the impact of the lack of session sharing.`),
    optional('tune.ssl.force-private-cache'),
);
const tuneSslMaxrecord = makeEntry(
    "tune.ssl.maxrecord",
    z.number().describe(`<number>: Sets the maximum amount of bytes passed to SSL_write() at a time. Default value 0 means there is no limit. Over SSL/TLS, the client can decipher the data only once it has received a full record. With large records, it means that clients might have to download up to 16kB of data before starting to process them. Limiting the value can improve page load times on browsers located over high latency or low bandwidth networks. It is suggested to find optimal values which fit into 1 or 2 TCP segments (generally 1448 bytes over Ethernet with TCP timestamps enabled, or 1460 when timestamps are disabled), keeping in mind that SSL/TLS add some overhead. Typical values of 1419 and 2859 gave good results during tests. Use "strace -e trace=write" to find the best value. HAProxy will automatically switch to this setting after an idle stream has been detected (see tune.idletimer above).`),
    passthrough('tune.ssl.maxrecord'),
);
const tuneSslDefaultDhParam = makeEntry(
    "tune.ssl.default-dh-param",
    z.number().describe(`<number>: Sets the maximum size of the Diffie-Hellman parameters used for generating the ephemeral/temporary Diffie-Hellman key in case of DHE key exchange. The final size will try to match the size of the server's RSA (or DSA) key (e.g, a 2048 bits temporary DH key for a 2048 bits RSA key), but will not exceed this maximum value. Default value if 1024. Only 1024 or higher values are allowed. Higher values will increase the CPU load, and values greater than 1024 bits are not supported by Java 7 and earlier clients. This value is not used if static Diffie-Hellman parameters are supplied either directly in the certificate file or by using the ssl-dh-param-file parameter.`),
    passthrough('tune.ssl.default-dh-param'),
);
const tuneSslSslCtxCacheSize = makeEntry(
    "tune.ssl.ssl-ctx-cache-size",
    z.number().describe(`<number>: Sets the size of the cache used to store generated certificates to <number> entries. This is a LRU cache. Because generating a SSL certificate dynamically is expensive, they are cached. The default cache size is set to 1000 entries.`),
    passthrough('tune.ssl.ssl-ctx-cache-size'),
);
const tuneSslCaptureCipherlistSize = makeEntry(
    "tune.ssl.capture-cipherlist-size",
    z.number().describe(`<number>: Sets the maximum size of the buffer used for capturing client-hello cipher list. If the value is 0 (default value) the capture is disabled, otherwise a buffer is allocated for each SSL/TLS connection.`),
    passthrough('tune.ssl.capture-cipherlist-size'),
);
const tuneVarsGlobalMaxSize = makeEntry(
    "tune.vars.global-max-size",
    z.number(),
    passthrough('tune.vars.global-max-size'),
);
const tuneVarsProcMaxSize = makeEntry(
    "tune.vars.proc-max-size",
    z.number(),
    passthrough('tune.vars.proc-max-size'),
);
const tuneVarsReqresMaxSize = makeEntry(
    "tune.vars.reqres-max-size",
    z.number(),
    passthrough('tune.vars.reqres-max-size'),
);
const tuneVarsSessMaxSize = makeEntry(
    "tune.vars.sess-max-size",
    z.number(),
    passthrough('tune.vars.sess-max-size'),
);
const tuneVarsTxnMaxSize = makeEntry(
    "tune.vars.txn-max-size",
    z.number(),
    passthrough('tune.vars.txn-max-size'),
);
const tuneZlibMemlevel = makeEntry(
    "tune.zlib.memlevel",
    z.number().describe(`<number>: Sets the memLevel parameter in zlib initialization for each session. It defines how much memory should be allocated for the internal compression state. A value of 1 uses minimum memory but is slow and reduces compression ratio, a value of 9 uses maximum memory for optimal speed. Can be a value between 1 and 9. The default value is 8.`),
    passthrough('tune.zlib.memlevel'),
);
const tuneZlibWindowsize = makeEntry(
    "tune.zlib.windowsize",
    z.number().describe(`<number>: Sets the window size (the size of the history buffer) as a parameter of the zlib initialization for each session. Larger values of this parameter result in better compression at the expense of memory usage. Can be a value between 8 and 15. The default value is 15.`),
    passthrough('tune.zlib.windowsize'),
);
const debug = makeEntry(
    "debug",
    z.boolean().describe(`Enables debug mode which dumps to stdout all exchanges, and disables forking into background. It is the equivalent of the command-line argument "-d". It should never be used in a production configuration since it may prevent full system startup.`),
    optional('debug'),
);
const quiet = makeEntry(
    "quiet",
    z.boolean().describe(`Do not display any message during startup. It is equivalent to the command- line argument "-q".`),
    optional('quiet'),
);

const generator = (v: Record<string, any>) => `global\n${makeGenerator(
    caBase,
    chroot,
    crtBase,
    cpuMap,
    daemon,
    description,
    deviceatlasJsonFile,
    deviceatlasLogLevel,
    deviceatlasSeparator,
    deviceatlasPropertiesCookie,
    externalCheck,
    gid,
    group,
    hardStopAfter,
    h1CaseAdjust,
    h1CaseAdjustFile,
    log,
    logTag,
    logSendHostname,
    luaLoad,
    masterWorker,
    mworkerMaxReloads,
    nbproc,
    nbthread,
    node,
    pidFile,
    presetenv,
    setenv,
    stats,
    unixBind,
    sslEngine,
    resetenv,
    uid,
    ulimitN,
    user,
    setDumpable,
    sslDefaultBindCiphers,
    sslDefaultBindCiphersuites,
    sslDefaultBindOptions,
    sslDefaultServerCiphers,
    sslDefaultServerCiphersuites,
    sslDefaultServerOptions,
    sslDhParamFile,
    sslServerVerify,
    unsetenv,
    fiftyOneDegreesDataFile,
    fiftyOneDegreesPropertyNameList,
    fiftyOneDegreesPropertySeparator,
    fiftyOneDegreesCacheSize,
    wurflDataFile,
    wurflInformationList,
    wurflInformationListSeparator,
    wurflPathFile,
    wurflCacheSize,
    maxSpreadChecks,
    maxconn,
    maxconnrate,
    maxcomprate,
    maxcompcpuusage,
    maxpipes,
    maxsessrate,
    maxsslconn,
    maxsslrate,
    maxzlibmem,
    noepoll,
    nokqueue,
    noevports,
    nopoll,
    nosplice,
    nogetaddrinfo,
    noreuseport,
    profilingTasks,
    spreadChecks,
    serverStateBase,
    serverStateFile,
    sslModeAsync,
    tuneBuffersLimit,
    tuneBuffersReserve,
    tuneBufsize,
    tuneChksize,
    tuneCompMaxlevel,
    tuneFailAlloc,
    tuneH2HeaderTableSize,
    tuneH2InitialWindowSize,
    tuneH2MaxConcurrentStreams,
    tuneH2MaxFrameSize,
    tuneHttpCookielen,
    tuneHttpLogurilen,
    tuneHttpMaxhdr,
    tuneIdletimer,
    tuneListenerMultiQueue,
    tuneLuaForcedYield,
    tuneLuaMaxmem,
    tuneLuaSessionTimeout,
    tuneLuaTaskTimeout,
    tuneLuaServiceTimeout,
    tuneMaxaccept,
    tuneMaxpollevents,
    tuneMaxrewrite,
    tunePatternCacheSize,
    tunePipesize,
    tunePoolHighFdRatio,
    tunePoolLowFdRatio,
    tuneRcvbufClient,
    tuneRcvbufServer,
    tuneRecv_enough,
    tuneRunqueueDepth,
    tuneSndbufClient,
    tuneSndbufServer,
    tuneSslCachesize,
    tuneSslLifetime,
    tuneSslForcePrivateCache,
    tuneSslMaxrecord,
    tuneSslDefaultDhParam,
    tuneSslSslCtxCacheSize,
    tuneSslCaptureCipherlistSize,
    tuneVarsGlobalMaxSize,
    tuneVarsProcMaxSize,
    tuneVarsReqresMaxSize,
    tuneVarsSessMaxSize,
    tuneVarsTxnMaxSize,
    tuneZlibMemlevel,
    tuneZlibWindowsize,
    debug,
    quiet,
)(v).split('\n').map((e) => `\t${e}`).join('\n')}`

export default {
    generator,
    validator: z.object({
        ...entryToObject(caBase),
        ...entryToObject(chroot),
        ...entryToObject(crtBase),
        ...entryToObject(cpuMap),
        ...entryToObject(daemon),
        ...entryToObject(description),
        ...entryToObject(deviceatlasJsonFile),
        ...entryToObject(deviceatlasLogLevel),
        ...entryToObject(deviceatlasSeparator),
        ...entryToObject(deviceatlasPropertiesCookie),
        ...entryToObject(externalCheck),
        ...entryToObject(gid),
        ...entryToObject(group),
        ...entryToObject(hardStopAfter),
        ...entryToObject(h1CaseAdjust),
        ...entryToObject(h1CaseAdjustFile),
        ...entryToObject(log),
        ...entryToObject(logTag),
        ...entryToObject(logSendHostname),
        ...entryToObject(luaLoad),
        ...entryToObject(masterWorker),
        ...entryToObject(mworkerMaxReloads),
        ...entryToObject(nbproc),
        ...entryToObject(nbthread),
        ...entryToObject(node),
        ...entryToObject(pidFile),
        ...entryToObject(presetenv),
        ...entryToObject(setenv),
        ...entryToObject(stats),
        ...entryToObject(unixBind),
        ...entryToObject(sslEngine),
        ...entryToObject(resetenv),
        ...entryToObject(uid),
        ...entryToObject(ulimitN),
        ...entryToObject(user),
        ...entryToObject(setDumpable),
        ...entryToObject(sslDefaultBindCiphers),
        ...entryToObject(sslDefaultBindCiphersuites),
        ...entryToObject(sslDefaultBindOptions),
        ...entryToObject(sslDefaultServerCiphers),
        ...entryToObject(sslDefaultServerCiphersuites),
        ...entryToObject(sslDefaultServerOptions),
        ...entryToObject(sslDhParamFile),
        ...entryToObject(sslServerVerify),
        ...entryToObject(unsetenv),
        ...entryToObject(fiftyOneDegreesDataFile),
        ...entryToObject(fiftyOneDegreesPropertyNameList),
        ...entryToObject(fiftyOneDegreesPropertySeparator),
        ...entryToObject(fiftyOneDegreesCacheSize),
        ...entryToObject(wurflDataFile),
        ...entryToObject(wurflInformationList),
        ...entryToObject(wurflInformationListSeparator),
        ...entryToObject(wurflPathFile),
        ...entryToObject(wurflCacheSize),
        ...entryToObject(maxSpreadChecks),
        ...entryToObject(maxconn),
        ...entryToObject(maxconnrate),
        ...entryToObject(maxcomprate),
        ...entryToObject(maxcompcpuusage),
        ...entryToObject(maxpipes),
        ...entryToObject(maxsessrate),
        ...entryToObject(maxsslconn),
        ...entryToObject(maxsslrate),
        ...entryToObject(maxzlibmem),
        ...entryToObject(noepoll),
        ...entryToObject(nokqueue),
        ...entryToObject(noevports),
        ...entryToObject(nopoll),
        ...entryToObject(nosplice),
        ...entryToObject(nogetaddrinfo),
        ...entryToObject(noreuseport),
        ...entryToObject(profilingTasks),
        ...entryToObject(spreadChecks),
        ...entryToObject(serverStateBase),
        ...entryToObject(serverStateFile),
        ...entryToObject(sslModeAsync),
        ...entryToObject(tuneBuffersLimit),
        ...entryToObject(tuneBuffersReserve),
        ...entryToObject(tuneBufsize),
        ...entryToObject(tuneChksize),
        ...entryToObject(tuneCompMaxlevel),
        ...entryToObject(tuneFailAlloc),
        ...entryToObject(tuneH2HeaderTableSize),
        ...entryToObject(tuneH2InitialWindowSize),
        ...entryToObject(tuneH2MaxConcurrentStreams),
        ...entryToObject(tuneH2MaxFrameSize),
        ...entryToObject(tuneHttpCookielen),
        ...entryToObject(tuneHttpLogurilen),
        ...entryToObject(tuneHttpMaxhdr),
        ...entryToObject(tuneIdletimer),
        ...entryToObject(tuneListenerMultiQueue),
        ...entryToObject(tuneLuaForcedYield),
        ...entryToObject(tuneLuaMaxmem),
        ...entryToObject(tuneLuaSessionTimeout),
        ...entryToObject(tuneLuaTaskTimeout),
        ...entryToObject(tuneLuaServiceTimeout),
        ...entryToObject(tuneMaxaccept),
        ...entryToObject(tuneMaxpollevents),
        ...entryToObject(tuneMaxrewrite),
        ...entryToObject(tunePatternCacheSize),
        ...entryToObject(tunePipesize),
        ...entryToObject(tunePoolHighFdRatio),
        ...entryToObject(tunePoolLowFdRatio),
        ...entryToObject(tuneRcvbufClient),
        ...entryToObject(tuneRcvbufServer),
        ...entryToObject(tuneRecv_enough),
        ...entryToObject(tuneRunqueueDepth),
        ...entryToObject(tuneSndbufClient),
        ...entryToObject(tuneSndbufServer),
        ...entryToObject(tuneSslCachesize),
        ...entryToObject(tuneSslLifetime),
        ...entryToObject(tuneSslForcePrivateCache),
        ...entryToObject(tuneSslMaxrecord),
        ...entryToObject(tuneSslDefaultDhParam),
        ...entryToObject(tuneSslSslCtxCacheSize),
        ...entryToObject(tuneSslCaptureCipherlistSize),
        ...entryToObject(tuneVarsGlobalMaxSize),
        ...entryToObject(tuneVarsProcMaxSize),
        ...entryToObject(tuneVarsReqresMaxSize),
        ...entryToObject(tuneVarsSessMaxSize),
        ...entryToObject(tuneVarsTxnMaxSize),
        ...entryToObject(tuneZlibMemlevel),
        ...entryToObject(tuneZlibWindowsize),
        ...entryToObject(debug),
        ...entryToObject(quiet),
    })
        .partial()
        .strict(),
}
