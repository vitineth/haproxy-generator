import {z, ZodTypeAny} from "zod";

export type GeneratorType<V extends z.ZodTypeAny> = ((k: z.infer<V>) => string);

export type Entry<V extends z.ZodTypeAny, F extends GeneratorType<V>, K extends string> = { key: K, validator: V, generator: F };
export const makeEntry = <V extends z.ZodTypeAny, F extends ((k: z.infer<V>) => string), K extends string>(key: K, v: V, f: F): Entry<V, F, K> => ({
    validator: v,
    generator: f,
    key,
});

export const entryToObject = <V extends z.ZodTypeAny, F extends GeneratorType<V>, K extends string, E extends Entry<V, F, K>>(e: E): Record<K, V> => {
    const x = e.key;
    return {[x]: e.validator} as (Record<K, V>)
};
export const makeGenerator = (...entries: Entry<any, any, any>[]) => {
    return (obj: Record<string, any>) => {
        const keys = Object.keys(obj);
        const asObj = Object.fromEntries(
            entries.map((e) => [e.key, e.generator]),
        ) satisfies Record<string, GeneratorType<any>>;

        let result = '';
        for (const k of keys) {
            if (Object.hasOwn(asObj, k)) result += `\n${asObj[k](obj[k])}`
        }

        return result;
    }
}
export const stringZod = z.object({
    weak: z.string(),
}).or(z.object({
    strong: z.string(),
})).or(z.string());

export const cpuMapRegex = /(auto:)?(odd|even|all|([0-9]+?(-([0-9]+)?)?))(\/(odd|even|all|([0-9]+?(-([0-9]+)?)?)))?\s+(odd|even|all|([0-9]+(-([0-9]+)?)?))(( (odd|even|all|([0-9]+(-([0-9]+)?)?)))+)?/gm;
export const timeRegex = /[0-9]+(us|ms|s|m|h|d)/;
export const sizeRegex = /[0-9]+(k|m|g)/;

export const timeZod = z.number().or(z.string().regex(timeRegex));
export const sizeZod = z.number().or(z.string().regex(sizeRegex));

export const stringToString = (s: z.infer<typeof stringZod>) => {
    if (typeof (s) === 'string') return s;
    if ('weak' in s) return `"${s.weak}"`;
    return `'${s.strong}'`;
}


export const stringArray = z.array(stringZod);

export const passthrough = (k: string) => ((x: z.infer<typeof stringZod> | number) => k + " " + (typeof (x) === 'number' ? String(x) : stringToString(x)));
export const optional = (v: string) => ((x: boolean) => x ? v : '');
export const stringArrayPassthrough = (k: string) => ((v: z.infer<typeof stringArray>) => v.map((e) => `${k} ${stringToString(e)}`).join('\n'));

export const indent = (s: string) =>  s.split('\n').map((e) => `\t${e}`).join('\n');

export const ifUnlessCondition = (s: {if: z.infer<typeof stringZod>} | {unless: z.infer<typeof stringZod>} | undefined) => {
    if (!s) return '';
    if ('if' in s) return `if ${stringToString(s.if)}`;
    return `unless ${stringToString(s.unless)}`;
}
