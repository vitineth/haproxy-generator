import {string, z} from "zod";
import {entryToObject, indent, makeEntry, passthrough, stringToString, stringZod} from "./utils";

const name = makeEntry(
    'name',
    stringZod.describe(`<listname>: Creates new userlist with name <listname>. Many independent userlists can be used to store authentication & authorization data for independent customers.`),
    passthrough('userlist'),
);

const group = makeEntry(
    'group',
    z.array(stringZod.or(z.object({
        groupname: stringZod,
        users: z.array(stringZod),
    }))).optional().describe(`<groupname> [users <user>,<user>,(...)]: Adds group <groupname> to the current userlist. It is also possible to attach users to this group by using a comma separated list of names proceeded by "users" keyword.`),
    (v) => v ? v.map((e) => (typeof (e) === 'object' && 'groupname' in e) ? `group ${stringToString(e.groupname)} ${e.users ? 'users ' + e.users.map(stringToString).join(',') : ''}` : `group ${stringToString(e)}`).join('\n') : '',
)

const user = makeEntry(
    'user',
    z.array(stringZod.or(z.object({
        username: stringZod,
        password: z.object({
            mode: z.enum(['password', 'insecure-password']),
            password: stringZod,
        }).optional(),
        groups: z.array(stringZod).optional(),
    }))).optional().describe(`<username> [password | insecure-password <password>] [groups <group>,<group>,(...)]: Adds user <username> to the current userlist. Both secure (encrypted) and insecure (unencrypted) passwords can be used. Encrypted passwords are evaluated using the crypt(3) function, so depending on the system's capabilities, different algorithms are supported. For example, modern Glibc based Linux systems support MD5, SHA-256, SHA-512, and, of course, the classic DES-based method of encrypting passwords. Attention: Be aware that using encrypted passwords might cause significantly increased CPU usage, depending on the number of requests, and the algorithm used. For any of the hashed variants, the password for each request must be processed through the chosen algorithm, before it can be compared to the value specified in the config file. Most current algorithms are deliberately designed to be expensive to compute to achieve resistance against brute force attacks. They do not simply salt/hash the clear text password once, but thousands of times. This can quickly become a major factor in haproxy's overall CPU consumption!`),
    (v) => v ? v.map((e) => {
        if (typeof (e) === 'object' && 'username' in e) {
            let out = `user ${stringToString(e.username)}`;
            if (e.password) out += ` ${e.password.mode} ${stringToString(e.password.password)}`;
            if (e.groups) out += ` groups ${e.groups.map(stringToString).join(',')}`;

            return out;
        } else {
            return `user ${stringToString(e)}`;
        }
    }).join('\n') : '',
)

const userlistEntry = z.array(z.object({
    name: name.validator,
    group: group.validator,
    user: user.validator,
}));

const userlists = makeEntry(
    'userlists',
    userlistEntry,
    (v) => v.map((e) => {
        let out = name.generator(e.name);
        if (e.group) out += `\n${indent(group.generator(e.group))}`;
        if (e.user) out += `\n${indent(user.generator(e.user))}`;
        return out;
    }).join('\n'),
)

export default {
    generator: userlists.generator,
    validator: userlists.validator,
}
