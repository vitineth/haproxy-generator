# haproxy-generator

This takes a JSON file and converts it to the format used by haproxy. It aims to copy most of the spec but with more
relaxed rules as haproxy will do the rest of the config. This is designed so I can make scripts to spit out a haproxy
config in JSON and parse / manipulate it with commands and then use this to generate a real config.

The spec isn't super clear in places about what is required and whats not, what directives can be repeated and which
can't so there are likely a lot of omissions and annoyingly a lot of things need to be provided as arrays (which is
something I want to fix on another pass) but it works for the configuration I use day-to-day (which is very limited).

As I run into issues in my own setup I will gradually update it. If you actually want to use this you'll need to get
reasonably good at reading zod error outputs as the validation error messages are bubbled right up and can be a bit
misleading in the case of unions. At some point soon I will add an example config but I need to come up with some fake
data as an example. If anyone has complex configs I can test with, it would be great to receive them to recreate and
check outputs.

## Ordering

Some things in haproxy configs rely on correct ordering and obviously JSON doesn't allow repeating and clear ordering.
Therefore in the proxies sections, they can be defined as a single object, or as an array of objects. In the case of an
array of objects they will be processed in order allowing you to require some directives to come after others. 

## Running

```bash
$ npm run generate
```

This expects there to be a haproxy.config.json file in the current folder.
